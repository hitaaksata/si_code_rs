  <?php
  include("include/connect.php"); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="global_assets/js/main/jquery.min.js"></script>
	<script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->


	<script src="assets/js/app.js"></script>	
	<script type="text/javascript" language="javascript" src="include/ajaxrequest.js"></script>
	<!-- /theme JS files -->

	<script type="text/javascript">
		function jumpTo (link){
		   var new_url=link;
		   if ((new_url != "")  &&  (new_url != null))
		   window.location=new_url;
		}

		function display_c(){
			var refresh=1000; // Refresh rate in milli seconds
			mytime=setTimeout('display_ct()',refresh)
		}

		function display_ct() {
			var x = new Date()
			var x1=x.toUTCString();// changing the display to UTC string
			document.getElementById('dt').innerHTML = x1;
			tt=display_c();
		}

		jQuery(document).ready(function(){
			jQuery("#NIP").keyup(function(event){
				if(event.keyCode == 13){
					MyAjaxRequest('valid_nip','include/process.php?NIP=','NIP');
					jQuery("#PWD").focus();
				}
			});

			$("#LOGIN").click(function(){
				$("#frm").submit();
			})
			
			jQuery("#PWD").keyup(function(event){		
				if(event.keyCode == 13){			
					//MyAjaxRequest('valid_pwd','include/process.php?PWD=','PWD');
					jQuery('#frm').submit();
				}
			});

		});
	</script>
	<style type="text/css">
		.form-group {
    margin-bottom: 1.25rem;
}
	</style>

</head>

<body onload=display_ct();>

	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="index.html" class="d-inline-block">
				<img src="global_assets/images/logo_light.png" alt="">
			</a>
		</div>

		

		<div class="collapse navbar-collapse" id="navbar-mobile">
			

			<span class="navbar-text ml-md-3 mr-md-auto">
				<span class="badge bg-success"></span>
			</span>

			<ul class="navbar-nav">
				<li class="nav-item dropdown dropdown-user">
					<span id="dt"></span>
				</li>
			</ul>
		</div>
		

		
	</div>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">

				<!-- Login card -->
				<form class="login-form" name="frm" id="frm" action="<?= $base_url;?>include/user_level.php" method="post">
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								<i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="mb-0">Login to your account</h5>
								<span class="d-block text-muted">Your credentials</span>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="text" class="form-control" placeholder="Username" id="NIP" name="USERNAME" onBlur="javascript: MyAjaxRequest('valid_nip','include/process.php?NIP=','NIP');return false;" >
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
								<div id="valid_nip"></div>
								
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" class="form-control" placeholder="Password" name="PWD" id="PWD">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
								<div id="valid_pwd"></div>
							</div>

							<div class="form-group">
								<select class="form-control" name="shift">
									<option value="1">Shift 1</option>
									<option value="2">Shift 2</option>
									<option value="2">Shift 3</option>
								</select>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-primary btn-block"  name="LOGIN" id="LOGIN">Sign in <i class="icon-circle-right2 ml-2"></i></button>
							</div>

							
						</div>
					</div>
				</form>
				<!-- /login card -->

			</div>
			<!-- /content area -->


			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2019. <a href="#">SISTEM INFORMASI MANAJEMEN RUMAH SAKIT</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</span>

					
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>
