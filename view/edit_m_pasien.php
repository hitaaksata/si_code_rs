

<?php
$sql	= mysql_query('select * from m_pasien where NOMR = "'.$_GET['NOMR'].'"');
$data	= mysql_fetch_array($sql);
  //print_r($data);
  genStartCard("IDENTITAS PASIEN");
  genBodyCard();
?>

<script type="text/javascript">
  $(document).ready(function(){
    $("#myform").validate({
      errorClass: 'validation-invalid-label',
            successClass: 'validation-valid-label',
            validClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            success: function(label) {
                label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {

                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo( element.parents('.form-check').parent() );
                }

                // Input with icons and Select2
                else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo( element.parent() );
                }

                // Input group, styled file input
                else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                    error.appendTo( element.parent().parent() );
                }

                // Other elements
                else {
                    error.insertAfter(element);
                }
            },
            rules: {},
            messages: {}
    });
  })
</script>
<form name="myform" id="myform" action="models/edit_pasien.php?edit=ok" method="post">
  <fieldset class="mb-3">
    <legend class="text-uppercase font-weight-bold">Identitas Pasien</legend>

    <div class="form-group row">
      <?php genLabel("No. RM"); ?>
      <div class="col-lg-3">
        <?php genInputText("xNOMR", "xNOMR", $data['NOMR'],"text","","disabled") ?>
         <?php genInputText("NOMRKEY", "NOMRKEY", $data['NOMR'],"hidden","","") ?>
      </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Awal Daftar"); ?>
      <div class="col-lg-2">
        <?php genInputText("awaldaftar", "awaldaftar", $data['TGLDAFTAR'],"text","","disabled") ?>
      </div>
    </div>

    <div class="form-group row">
            <?php genLabel("Cara Bayar"); ?>
        <div class="col-lg-8">
            <?php
            $ss = mysql_query('select * from m_carabayar order by ORDERS ASC');
            while ($ds = mysql_fetch_array($ss)) {
                if ($data['KDCARABAYAR'] == $ds['KODE']): $sel = "Checked";
                else: $sel = '';
                endif;

                echo '
          <div class="form-check form-check-inline">
            
            <label class="form-check-label">
              <input type="radio" class="form-check-input" name="KDCARABAYAR" id="carabayar_' . $ds['KODE'] . '" '.$sel.' value="' . $ds['KODE'] . '" required>
              ' . $ds['NAMA'] . '
            </label>
          </div>';
            }
            ?>
        </div>
    </div>
    
    <div class="form-group row">
      <?php genLabel("Nama Lengkap Pasien"); ?>
      <div class="col-lg-4">
        <?php 
          $nama = (!empty($data['NAMA']))?$data['NAMA']:"";
          genInputText("NAMA","NAMA",$nama,"text","","required");
        ?>
      </div>
      <div class="col-lg-2">
        <select class="form-control form-control-sm" name="CALLER" id="CALLER">
            <option selected="selected" value="">- Alias -</option>
            <option value="Tn" <? if($data['CALLER']=="Tn") echo "selected=selected"; ?>> Tn </option>
            <option value="Ny" <? if($data['CALLER']=="Ny") echo "selected=selected"; ?>> Ny </option>
            <option value="Nn" <? if($data['CALLER']=="Nn") echo "selected=selected"; ?>> Nn </option>
            <option value="An" <? if($data['CALLER']=="An") echo "selected=selected"; ?>> An </option>
        </select>
      </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Tempat/Tgl Lahir"); ?>
      <div class="col-lg-2">
        <?php 
          $TEMPAT = (!empty($data['TEMPAT']))?$data['TEMPAT']:"";
          genInputText("TEMPAT","TEMPAT",$TEMPAT,"text","","required"); 
        ?>
      </div>
      <div class="col-lg-2">
        <?php 
          $TGLLahir =(!empty($data['TGLLAHIR']))?$data['TGLLAHIR']:date('Y-m-d');
          genInputText("TGLLAHIR","TGLLAHIR",$TGLLahir, "date","","required");
        ?>
      </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Umur") ?>
      <div class="col-lg-3">
        <?php 
          if ($data['TGLLAHIR']==""){
            $a = datediff(date("Y/m/d"), date("Y/m/d"));
          }
          else {
             $a = datediff($data['TGLLAHIR'], date("Y/m/d"));
          }
          $val_umur = 'umur '.$a[years].' tahun '.$a[months].' bulan '.$a[days].' hari';
          genInputText("umur","umur",$val_umur);
        ?>
      </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Alamat Sekarang");  ?>
      <div class="col-lg-8">
        <?php 
          $alamatSekarang = (!empty($data['ALAMAT']))?$data['ALAMAT']:"";
          genInputText("ALAMAT","ALAMAT",$alamatSekarang,"text","","required");
        ?>
      </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Alamat KTP");  ?>
      <div class="col-lg-8">
        <?php 
          $alamatktp = (!empty($data['ALAMAT_KTP']))?$data['ALAMAT_KTP']:"";
          genInputText("ALAMAT_KTP","ALAMAT_KTP",$alamatktp);
        ?>
      </div>
    </div>

    <div class="form-group row">
        <?php genLabel("Provinsi");?>
        <div class="col-lg-4">
            <select class="form-control form-control-sm select2" name="KDPROVINSI" id="KDPROVINSI" required>
              <option value="">-Pilih-</option>
              <?php
                  $ss = mysql_query('select * from m_provinsi order by idprovinsi ASC');
                  while($ds = mysql_fetch_array($ss)){
                        if($data['KDPROVINSI'] == $ds['idprovinsi']): $sel = "selected=Selected"; else: $sel = ''; endif;
                        echo '<option value="'.$ds['idprovinsi'].'" '.$sel.' > '.$ds['namaprovinsi'].'</option>';
                  }
              ?>
            </select>
            <?php 
              genInputText("KOTAHIDDEN", "KOTAHIDDEN","","hidden"); 
              genInputText("KECAMATANHIDDEN", "KECAMATANHIDDEN","","hidden"); 
              genInputText("KELURAHANHIDDEN", "KELURAHANHIDDEN", "","hidden"); 
             ?>
        </div>
    </div>

    <div class="form-group row">
        <?php genLabel("Kabupaten / Kota") ?>
        <div class="col-lg-4">
          <div id="kotapilih">
            <select class="form-control form-control-sm select2" name="KOTA" id="KOTA" required>
                <option value="">-Pilih-</option>
                <?php
                  $ss = mysql_query('select * from m_kota where idprovinsi = "'.$data['KDPROVINSI'].'" order by idkota ASC');
                  while($ds = mysql_fetch_array($ss)){
                        if($data['KOTA'] == $ds['idkota']): $sel = "selected=Selected"; else: $sel = ''; endif;
                        echo '<option value="'.$ds['idkota'].'" '.$sel.' > '.$ds['namakota'].'</option>';
                  }
                ?>
            </select>
          </div>
        </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Kecamatan") ?>
      <div class="col-lg-4">
        <div id="kecamatanpilih">
          <select class="form-control form-control-sm select2" name="KDKECAMATAN" id="KDKECAMATAN" required>
              <option value="">-Pilih-</option>
              <?php
                $ss = mysql_query('select * from m_kecamatan where idkota = "'.$data['KOTA'].'" order by idkecamatan ASC');
                while($ds = mysql_fetch_array($ss)){
                      if($data['KDKECAMATAN'] == $ds['idkecamatan']): $sel = "selected=Selected"; else: $sel = ''; endif;
                      echo '<option value="'.$ds['idkecamatan'].'" '.$sel.' /> '.$ds['namakecamatan'].'</option>&nbsp;';
                }
              ?>
          </select>
        </div>
      </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Kelurahan") ?>
      <div class="col-lg-4">
        <div id="kelurahanpilih">
          <select class="form-control form-control-sm select2" name="KELURAHAN" id="KELURAHAN" required>
              <option value="">-Pilih-</option>
              <?php
                $ss = mysql_query('select * from m_kelurahan where idkecamatan = "'.$data['KDKECAMATAN'].'" order by idkelurahan ASC');
                while($ds = mysql_fetch_array($ss)){
                      if($data['KELURAHAN'] == $ds['idkelurahan']): $sel = "selected=Selected"; else: $sel = ''; endif;
                      echo '<option value="'.$ds['idkelurahan'].'" '.$sel.' /> '.$ds['namakelurahan'].'</option>&nbsp;';
                }
              ?>
          </select>
        </div>
      </div>
    </div>


    <div class="form-group row">
      <?php     genLabel("No. telepon / HP") ?>
      <div class="col-lg-3">
          <?php 
            $notelp = (!empty($data['NOTELP']))? $data['NOTELP']:"";
            genInputText("NOTELP", "notelp", $notelp); 
           ?>
      </div>
    </div>
    
    <div class="form-group row">
        <?php genLabel("No. KTP") ?>
        <div class="col-lg-3">
            <?php 
                        $noKTP = (!empty($_GET['NOKTP']))?$_GET['NOKTP']:"";
                       genInputText("NOKTP", "NOKTP", $noKTP);
            ?>
        </div>
    </div>
    
    <div class="form-group row">
        <?php genLabel("Jenis Kelamin") ?>
        <div class="col-lg-8">
              <?php 
                    
                    if(strtoupper($data['JENISKELAMIN'])=="L"){
                        $JK_L = "checked";
                        $JK_P ="";
                    }elseif (strtoupper($data['JENISKELAMIN'])=="P") {
                        $JK_L = "";
                        $JK_P="checked";
                    }
              ?>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                  
                    <input type="radio"  name="JENISKELAMIN" id="JENISKELAMIN_L" value="L" class="form-check-input" <?= $JK_L ?> required>
                    Laki - Laki
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                     <input type="radio"  name="JENISKELAMIN" id="JENISKELAMIN_P" value="P" class="form-check-input" <?= $JK_P ?> required>
                    Perempuan
                </label>
            </div>
        </div>
    </div>

    <div class="form-group row">
      <?php genLabel(" Status Perkawinan") ?>
      <div class="col-lg-8">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
               <input type="radio" name="STATUS" id="status_1" class="form-check-input" value="1" <? if($data['STATUS']=="1"){echo "Checked";}?>/>
              Belum Menikah
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input type="radio" name="STATUS" id="status_2" value="2" class="form-check-input" <? if($data['STATUS']=="2") echo "Checked";?> />
               Menikah
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input type="radio" name="STATUS" id="status_3" value="3" class="form-check-input" <? if($data['STATUS']=="3") echo "Checked";?>/>
                Janda / Duda
            </label>
        </div>
      </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Pendidikan Terakhir") ?>
      <div class="col-lg-8">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_1" value="1" class="form-check-input" <? if($data['PENDIDIKAN']=="1"){echo "Checked";}?> />
              SD
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
               <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_2" value="2" class="form-check-input" <? if($data['PENDIDIKAN']=="2") echo "Checked";?> />
              SLTP
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_3" value="3" class="form-check-input" <? if($data['PENDIDIKAN']=="3") echo "Checked";?> />
              SMU / SMK
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_5" value="5" class="form-check-input" <? if($data['PENDIDIKAN']=="5") echo "Checked";?> />
              Universitas
            </label>
        </div>
      </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Agama") ?>
      <div class="col-lg-8">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input type="radio" name="AGAMA" id="AGAMA_1" value="1" class="form-check-input" <? if($data['AGAMA']=="1") echo "Checked";?> />
              Islam
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input type="radio" name="AGAMA" id="AGAMA_2" value="2" class="form-check-input" <? if($data['AGAMA']=="2") echo "Checked";?>/>
              Kristen Protestan
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
             <input type="radio" name="AGAMA" id="AGAMA_3" value="3" class="form-check-input" <? if($data['AGAMA']=="3"){echo "Checked";}?>/>
              Katholik
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
             <input type="radio" name="AGAMA" id="AGAMA_4" value="4" class="form-check-input" <? if($data['AGAMA']=="4") echo "Checked";?>/>
              Hindu
            </label>
        </div>
        <div class="form-check form-check-inline">
            <label class="form-check-label">
             <input type="radio" name="AGAMA" id="AGAMA_5" value="5" class="form-check-input" <? if($data['AGAMA']=="5") echo "Checked";?>/>
              Budha 
            </label>
        </div>
      </div>
    </div>

  </fieldset>

  <fieldset class="mb-3">
    <legend class="text-uppercase font-weight-bold">DATA PENDUKUNG</legend>
    <div class="form-group row">
        <?php genLabel("Nama Suami / Orang Tua ") ?>
        <div class="col-lg-4">
            <?php 
                        $nosuami_ortu = (!empty($data['SUAMI_ORTU']))?$data['SUAMI_ORTU']:"";
                       genInputText("SUAMI_ORTU", "SUAMI_ORTU", $nosuami_ortu);
            ?>
        </div>
    </div>
    
     <div class="form-group row">
        <?php genLabel("Pekerjaan Pasien / Orang Tua ") ?>
        <div class="col-lg-2">
            <?php 
                        $pekerjaan = (!empty($data['PEKERJAAN']))? $data['PEKERJAAN']:"";
                       genInputText("PEKERJAAN", "PEKERJAAN", $pekerjaan);
            ?>
        </div>
    </div>
    
    <div class="form-group row">
        <?php genLabel("Nama Penanggung Jawab ") ?>
        <div class="col-lg-4">
            <?php 
                        $nama_penanggungjawab =(!empty($data['nama_penanggungjawab']))?$data['nama_penanggungjawab']:""; 
                       genInputText("nama_penanggungjawab", "nama_penanggungjawab", $nama_penanggungjawab);
            ?>
        </div>
    </div>
    
    <div class="form-group row">
        <?php genLabel("Hubungan Dengan Pasien ") ?>
        <div class="col-lg-2">
            <?php 
                       $hubungan_penanggungjawab =(!empty($data['hubungan_penanggungjawab']))?$data['hubungan_penanggungjawab']:""; 
                       genInputText("hubungan_penanggungjawab", "hubungan_penanggungjawab", $hubungan_penanggungjawab);
            ?>
        </div>
    </div>
    
    <div class="form-group row">
        <?php genLabel("Alamat ") ?>
        <div class="col-lg-8">
            <?php 
                       $alamat_penanggungjawab =(!empty($data['alamat_penanggungjawab']))?$data['alamat_penanggungjawab']:""; 
                       genInputText("alamat_penanggungjawab", "alamat_penanggungjawab", $alamat_penanggungjawab);
            ?>
        </div>
    </div>
    
      <div class="form-group row">
        <?php genLabel("No Telepon / HP ") ?>
        <div class="col-lg-3">
            <?php 
                       $phone_penanggungjawab =(!empty($data['phone_penanggungjawab']))?$data['phone_penanggungjawab']:""; 
                       genInputText("phone_penanggungjawab", "phone_penanggungjawab", $phone_penanggungjawab);
            ?>
        </div>
    </div>
  </fieldset>
  <?php genBodyCardEnd(); ?>
  <div class="card-footer">
    <div class="d-flex justify-content-between align-items-center">
      <button type="button"  onClick="history.go(0)"  class="btn btn-light">Batal</button>
      <div class="d-inline-flex">
        <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-left"><b><i class="icon-floppy-disk"></i></b> Simpan</button>
        
      </div>
      
    </div>
  </div>
</form>


<?php genEndCard(); ?>
<script src="assets/js/global.js"></script>