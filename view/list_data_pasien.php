<?php
require_once('komponen/ps_pagination_x.php');
$link="?link=".$_GET['link'];

$search = "";
$searchkey = "";
$searchfield ="";

//print_r($_POST);

if(!empty($_POST['searchkey'])) {
    $searchkey = $_POST['searchkey'];
}

if(!empty($_POST['searchfield'])) {
    $searchfield = $_POST['searchfield'];
}

if($searchkey!="") {
    if($searchfield=="nomr") {
        $search = " WHERE NOMR like '%".$searchkey."%'";
    }
    if($searchfield=="nama") {
        $search = " WHERE NAMA like '%".$searchkey."%'";
    }
    if($searchfield=="alamat") {
        $search = " WHERE ALAMAT like '%".$searchkey."%'";
    }
    if($searchfield=="telepon") {
        $search = " WHERE NOTELP like '%".$searchkey."%'";
    }
    if($searchfield=="tgllahir") {
        $search = " WHERE TGLLAHIR like '%".$searchkey."%'";
    }
    if($searchfield=="noktp") {
        $search = " WHERE NOKTP like '%".$searchkey."%'";
    }
    if($searchfield=="tgldaftar") {
        $search = " WHERE TGLDAFTAR like '%".$searchkey."%'";
    }
    if($searchfield=="namasuami_orangtua") {
        $search = " WHERE SUAMI_ORTU like '%".$searchkey."%'";
    }
}

$order=" order by ";
if(!empty($_POST['orderby'])) {
    $orderby = $_POST['orderby'];

    if($orderby=="nomr") {
        $order=$order."NOMR";
    }
    if($orderby=="nama") {
        $order=$order."NAMA";
    }
    if($orderby=="alamat") {
        $order=$order."ALAMAT";
    }
    if($orderby=="telepon") {
        $order=$order."NOTELP";
    }
    if($orderby=="tgllahir") {
        $order=$order."TGLLAHIR";
    }
    if($orderby=="noktp") {
        $order=$order."NOKTP";
    }
    if($orderby=="tgldaftar") {
        $order=$order."TGLDAFTAR";
    }
    if($orderby=="namasuami_orangtua") {
        $order=$order."SUAMI_ORTU";
    }
}else {
    $order = $order."NOMR";
}
 ?>

<?php 
    genStartCard("LIST DATA PASIEN");
    genBodyCard();
?>
<form name="cari" method="post" class="mb-5" action="<?=$_SERVER['PHP_SELF'].$link;?>">
    <div class="form-group row">
       <?php genLabel("Cari","col-form-label col-lg-1") ?>
       <div class="col-lg-3">
           <?php genInputText("searchkey","searchkey",$searchkey); ?>
       </div>
       <?php genLabel("Berdasarkan","col-form-label col-lg-1") ?>
       <div class="col-lg-2">
           <select name="searchfield" id="searchfield" class="form-control form-control-sm">
                <option value="nomr" <? if($searchfield=="nomr") echo "selected"; ?>>nomr</option>
                <option value="nama" <? if($searchfield=="nama") echo "selected"; ?>>nama</option>
                <option value="alamat" <? if($searchfield=="alamat") echo "selected"; ?>>alamat</option>
                <option value="telepon" <? if($searchfield=="telepon") echo "selected"; ?>>telepon</option>
                <option value="tgllahir" <? if($searchfield=="tgllahir") echo "selected"; ?>>tgllahir</option>
                <option value="noktp" <? if($searchfield=="noktp") echo "selected"; ?>>noktp</option>
                <option value="tgldaftar" <? if($searchfield=="tgldaftar") echo "selected"; ?>>tgldaftar</option>
                <option value="namasuami_orangtua" <? if($searchfield=="namasuami_orangtua") echo "selected"; ?>>nama suami / orangtua</option>
           </select>
       </div>
       <?php genLabel("Sort","col-form-label col-lg-1") ?>
       <div class="col-lg-2">
           <select name="orderby" id="orderby" class="form-control form-control-sm">
                <option value="nomr" <? if($order=="nomr") echo "selected"; ?>>nomr</option>
                <option value="nama" <? if($order=="nama") echo "selected"; ?>>nama</option>
                <option value="alamat" <? if($order=="alamat") echo "selected"; ?>>alamat</option>
                <option value="telepon" <? if($order=="telepon") echo "selected"; ?>>telepon</option>
                <option value="tgllahir" <? if($order=="tgllahir") echo "selected"; ?>>tgllahir</option>
                <option value="noktp" <? if($order=="noktp") echo "selected"; ?>>noktp</option>
                <option value="tgldaftar" <? if($order=="tgldaftar") echo "selected"; ?>>tgldaftar</option>
                <option value="namasuami_orangtua" <? if($order=="namasuami_orangtua") echo "selected"; ?>>nama suami / orangtua</option>
           </select>
       </div>
       <div class="col-lg-2">
           <button type="submit" class="btn btn-sm btn-primary btn-labeled btn-labeled-left"><b><i class="icon-search4"></i></b> Cari</button>
           <a href="komponen/cetak.php?link=list_data_pasien&searchkey=<?=$searchkey;?>&searchfield=<?=$searchfield;?>&orderby=<?=$orderby;?>" target="_blank" class="btn btn-sm bg-slate btn-labeled btn-labeled-left ml-1"><b><i class="icon-printer2"></i></b>Print</a>
            
       </div>
    </div>
</form>
<div class="table-responsive mb-3">
    <table class="table table-sm table-bordered table-hover">
        <thead>
            <tr class="bg-teal-400">
                <th width="5%">NoRM</th>
                <th width="12%">Nama Pasien</th>
                <th width="9%"> TanggalLahir</th>
                <th width="15%">Alamat</th>
                <th width="18%">NO KTP</th>
                <th width="12%">Jenis Kelamin</th>
                <th width="8%">No telepon</th>
                <th width="14%">AwalDaftar</th>
                <th width="7%">Suami/Keluarga</th>
                <th width="7%">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $sql="SELECT a.* , DATE_FORMAT(TGLLAHIR,'%d/%m/%Y') as TGLLAHIR1, DATE_FORMAT(tgldaftar,'%d/%m/%Y') tgldaftar FROM m_pasien a ".$search.$order;
                $sql1="SELECT count(*) FROM m_pasien a ".$search.$order;

                //echo $sql;

                $pager = new PS_Pagination($connect, $sql, $sql1, 15, 5, "orderby=".$orderby."&searchkey=".$searchkey."&searchfield=".$searchfield, "index.php?link=21&");
                $rs = $pager->paginate();
                if(!$rs) die(mysql_error());
                while($data = mysql_fetch_array($rs)) {?>
                <tr>
                    <td><? echo $data['NOMR'];?></td>
                    <td><? echo $data['NAMA']; ?></td>
                    <td><? echo $data['TGLLAHIR1']; ?></td>
                    <td><? echo $data['ALAMAT']; ?></td>
                    <td><? echo $data['NOKTP']; ?></td>
                    <td><? if($data['JENISKELAMIN']=="l" || $data['JENISKELAMIN']=="L") {
                                echo"Laki-Laki";
                            }elseif($data['JENISKELAMIN']=="p" || $data['JENISKELAMIN']=="P") {
                                echo"Perempuan";
                            } ?></td>
                    <td><? echo $data['NOTELP']; ?></td>
                    <td><? echo $data['tgldaftar']; ?></td>
                    <td><? echo $data['SUAMI_ORTU']; ?></td>
                    <td>
                        <a href="?link=24&NOMR=<?=$data['NOMR'];?>" class="btn btn-sm bg-warning-400 btn-labeled btn-labeled-left"><b><i class="icon-pencil7"></i></b>Edit</a>
                    </td>
                </tr>

            <?php
                }        
            ?>
        </tbody>
    </table>
</div>
<div class="row mb-1">
    <div class="col-lg-12">
        <ul class="pagination align-self-center justify-content-center">
            <?php 
                echo $pager->renderFirst();

                //Display the link to previous page: <<
                //echo $pager->renderPrev();

                //Display page links: 1 2 3
                echo $pager->renderNav();

                //Display the link to next page: >>
                //echo $pager->renderNext();

//Display the link to last page: Last
                echo $pager->renderLast();
            ?>
            
        </ul>
    </div>
</div>
<div class="card-footer">
        <div class="d-flex justify-content-between align-items-center">
           
            <div class="d-inline-flex">
                <form method="post" action="komponen/excelexport.php" target="_blank">
                    <?php
        $qry_excel = "SELECT m_pasien.NOMR,
                         m_pasien.NAMA AS NAMA_PASIEN,
                         m_pasien.TEMPAT AS TMP_LAHIR,
                         m_pasien.TGLLAHIR AS TGL_LAHIR,
                         m_pasien.JENISKELAMIN AS JNS_KELAMIN,
                         m_pasien.ALAMAT,
                         m_pasien.KELURAHAN,
                         m_pasien.KDKECAMATAN AS KECAMATAN,
                         m_pasien.KOTA,
                         m_pasien.NOTELP AS NO_TELP,
                         m_pasien.NOKTP AS NO_KTP,
                         m_pasien.ALAMAT_KTP,
                         m_pasien.SUAMI_ORTU,
                         m_pasien.PEKERJAAN,
                         m_pasien.`STATUS`,
                         m_pasien.AGAMA,
                         m_pasien.PENDIDIKAN,
                         m_pasien.TGLDAFTAR TGL_PERTAMA_DAFTAR 
                   FROM m_pasien ".$search.$order;
        ?>
                    <input type="hidden" name="query" value="<?=$qry_excel?>" />
                    <input type="hidden" name="header" value="DATA PASIEN" />
                    <input type="hidden" name="filename" value="data_pasien" />
                    <button type="submit" class="btn bg-slate btn-labeled btn-labeled-left ml-1"><b><i class="icon-file-spreadsheet"></i></b>Export To Ms Excel Document</button>
                </form>
                
            </div>
            
        </div>
    </div>

<?php
    genBodyCardEnd();
    genEndCard();
?>



