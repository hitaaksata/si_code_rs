<?php unset($_SESSION['register_nomr']);
unset($_SESSION['register_nama']); ?>
<script type="text/javascript">

	$(document).ready(function(){

		$("#myform").validate({
			errorClass: 'validation-invalid-label',
            successClass: 'validation-valid-label',
            validClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            success: function(label) {
                label.addClass('validation-valid-label').text('Success.'); // remove to hide Success message
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {

                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo( element.parents('.form-check').parent() );
                }

                // Input with icons and Select2
                else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo( element.parent() );
                }

                // Input group, styled file input
                else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                    error.appendTo( element.parent().parent() );
                }

                // Other elements
                else {
                    error.insertAfter(element);
                }
            },
            rules: {},
            messages: {}
		});

		<?php if($_REQUEST['xNOMR'] == ''): ?> 
			$('#NOMR').attr('disabled','disabled').val('-automatic-');
			
		<? endif; ?>

		$('.statuspasien').change(function(){
			var status_val	= $(this).val();
			if(status_val == 1){
				/*$('#NOMR').attr('disabled','disabled').val('-automatic-');
				$('#PASIENBARU').val(1);
				$('#NAMA').val('');
				$('#TEMPAT').val('');
				$('#TGLLAHIR').val('');
				//$('#umur').val(newdata[5]);
				$('#ALAMAT').val('');
				$('#ALAMAT_KTP').val('');
				$('#KELURAHAN').val('');
				$('#KECAMATAN').val('');
				$('#KOTA').val('');
				$('#KDPROVINSI').val('');
				$('#notelp').val('');
				$('#NOKTP').val('');
				$('#SUAMI_ORTU').val('');
				$('#PEKERJAAN').val('');
				//$('#nama_penanggungjawab').val(newdata[14]);
				//$('#hubungan_penanggungjawab').val(newdata[15]);
				//$('#alamat_penanggungjawab').val(newdata[16]);
				//$('#phone_penanggungjawab').val(newdata[17]);
				$('#JENISKELAMIN_'+newdata[5]).removeAttr('checked');
				$('#status_'+newdata[15]).removeAttr('checked');
				$('#PENDIDIKAN_'+newdata[17]).removeAttr('checked');
				$('#AGAMA_'+newdata[16]).removeAttr('checked');
				$('#carabayar_'+newdata[18]).removeAttr('checked');
				$('.loader').hide();*/
				location.reload();
			}else{
				$('#NOMR').removeAttr('disabled').val('');
				$('#PASIENBARU').val(0);
			}
		});

		$('#NOMR').blur(function(){
			var nomr	= $(this).val();
			if(nomr != ''){
				$('.loader').show();
				$.get('<?php echo _BASE_; ?>include/process.php?psn='+nomr,function(data){
					newdata	= data.split("|");
					
					$('#NAMA').val(newdata[2]);
					$('#TEMPAT').val(newdata[3]);
					
					$('#TGLLAHIR').val(newdata[4]);
					//$('#umur').val(newdata[5]);
					$('#ALAMAT').val(newdata[6]);
					$('#ALAMAT_KTP').val(newdata[19]);
					//$('#KELURAHAN').val(newdata[7]);
					//$('#KECAMATAN').val(newdata[8]);
					$('#KELURAHANHIDDEN').val(newdata[7]);
					$('#KECAMATANHIDDEN').val(newdata[8]);
					$('#KOTAHIDDEN').val(newdata[9]);
					$('#KDPROVINSI').val(newdata[10]).change();
					//$('#KOTA').val(newdata[9]).change();
					$('#notelp').val(newdata[11]);
					$('#NOKTP').val(newdata[12]);
					$('#SUAMI_ORTU').val(newdata[13]);
					$('#PEKERJAAN').val(newdata[14]);
					$('#umur').val(newdata[20]);
					if(newdata[21]!=""){
						$('#CALLER option[value='+newdata[21]+']').attr('selected', 'selected');
					}
					
					//alert(newdata[5]);
					$('#nama_penanggungjawab').val(newdata[22]);
					$('#hubungan_penanggungjawab').val(newdata[23]);
					$('#alamat_penanggungjawab').val(newdata[24]);
					$('#phone_penanggungjawab').val(newdata[25]);
					$('#JENISKELAMIN_'+newdata[5]).attr('checked','checked');
					$('#status_'+newdata[15]).attr('checked','checked');
					$('#PENDIDIKAN_'+newdata[17]).attr('checked','checked');
					$('#AGAMA_'+newdata[16]).attr('checked','checked');
					$('#carabayar_'+newdata[18]).attr('checked','checked');
					
				});
			}
		});
		$('#kdpoly').change(function(){
			var val	= $(this).val();
			$('#loader_namadokter').show();
			$.post('<?php echo _BASE_;?>include/ajaxload.php',{kdpoly:val,load_dokterjaga:'true'},function(data){
				//var n = data.split("|");
				//$('#kddokter').val(n[0]);
				$('#listdokter_jaga').empty().append(data);
				//$('#loader_namadokter').hide();
				$(".select2").select2();
			});
		});
		//$('#carabayar_lain').hide();
		//$('#kdrujuk_lain').hide();
		$('.carabayar').click(function(){
			var val = $(this).val();
			if(val == 5){
				$('#carabayar_lain').show().addClass('required');
			}else{
				$('#carabayar_lain').hide().removeClass('required');
			}
		});
		$('.kdrujuk').click(function(){
			var val = $(this).val();
			if(val != 1){
				$('#kdrujuk_lain').show().addClass('required');
			}else{
				$('#kdrujuk_lain').hide().removeClass('required');
			}
		});



	});

	
</script>

<?php 
	genStartCard("IDENTITAS PASIEN"); 
	genBodyCard();
	//print_r($_GET);
?>
<form action="models/pendaftaran.php" method="post" name="myform" id="myform">
	<input type="hidden" name="SHIFT" value="<?= $_SESSION['shift'] ?>">
    <fieldset class="mb-3">
        <legend class="text-uppercase font-weight-bold">Pendaftaran Pasien</legend>

        <div class="form-group row">
            <input type="hidden" name="PASIENBARU" id="PASIENBARUS" value="1">
            <?php genLabel("Status Pasien") ?>
            <div class="col-lg-9">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input statuspasien" name="STATUSPASIEN" value="1" checked>
                        Pasien Baru
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input statuspasien" name="STATUSPASIEN" value="0">
                        Pasien Lama
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <?php genLabel("No. MR Pasien") ?>
            <div class="col-lg-3">
                <?php genInputText("NOMR", "NOMR", $_REQUEST['xNOMR']) ?>
            </div>
        </div>

        <div class="form-group row">
            <?php genLabel("Poli/Dokter yg Dituju") ?>
            <div class="col-lg-3">
                <select class="form-control select2" name="KDPOLY" id="kdpoly" required>
                    <option value=""> - Pilih Poliklinik - </option>
                    <?php
                    $sql = mysql_query('select * from m_poly order by nama asc');
                    while ($data = mysql_fetch_array($sql)) {
                        if ($_GET['KDPOLY'] == $data['kode']): $zx = 'selected="selected"';
                        else: $zx = '';
                        endif;
                        echo '<option value="' . $data['kode'] . '" ' . $zx . '>' . $data['nama'] . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-4" id="listdokter_jaga">
                <?php
            	if($_GET['KDPOLY'] != ''){
					$sqldokter	= mysql_query('select a.kddokter, b.NAMADOKTER from m_dokter_jaga a join m_dokter b on a.KDDOKTER = b.kddokter where a.kdpoly = "'.$_GET['KDPOLY'].'"');
					if(mysql_num_rows($sqldokter) > 0){
						echo '<select name="KDDOKTER" class="form-control form-control-sm select2">';
						while($datadok = mysql_fetch_array($sqldokter)){
							if($_GET['KDDOKTER'] == $datadok['kddokter']): $sel = 'selected="selected"'; else: $sel = ''; endif;
							echo '<option value="'.$datadok['kddokter'].'" '.$sel.'>'.$datadok['NAMADOKTER'].'</option>';
						}
						echo '</select>';
					}else{
						echo 'Tidak ada dokter jaga di poli tersebut';
					}
					#echo getDokterName($_GET['KDDOKTER']);
				}
				?>
            </div>
        </div>

        <div class="form-group row">
		<?php genLabel("Minta Rujukan") ?>
            <div class="col-lg-4">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="minta_rujukan" id="minta_rujukan" value="1">
                </div>
            </div>
        </div>

        <div class="form-group row">
                <?php genLabel("Tanggal Daftar"); ?>
            <div class="col-lg-2">
				
				<input type="text" class="form-control form-control-sm" name="TGLREG" id="TGLREG" disabled value="<?=date("d/m/Y | H:i")?>" name="">
				<input type='hidden' name='start_daftar' id='start_daftar' />
            </div>
        </div>

        <div class="form-group row">
                <?php genLabel("Cara Bayar"); ?>
            <div class="col-lg-8">
                <?php
                $ss = mysql_query('select * from m_carabayar order by ORDERS ASC');
                while ($ds = mysql_fetch_array($ss)) {
                    if ($_GET['KDCARABAYAR'] == $ds['KODE']): $sel = "Checked";
                    else: $sel = '';
                    endif;

                    echo '
							<div class="form-check form-check-inline">
								
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="KDCARABAYAR" id="carabayar_' . $ds['KODE'] . '" '.$sel.' value="' . $ds['KODE'] . '" required>
									' . $ds['NAMA'] . '
								</label>
							</div>';
                }
                ?>
            </div>
        </div>

        <div class="form-group row">
                <?php genLabel("Asal Pasien"); ?>
            <div class="col-lg-8">
                <?php
                $ss = mysql_query('select * from m_rujukan order by ORDERS ASC');
                while ($ds = mysql_fetch_array($ss)) {
                    if ($_GET['KDRUJUK'] == $ds['KODE']): $sel = "Checked";
                    else: $sel = '';
                    endif;

                    echo '
								<div class="form-check form-check-inline">
									
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="KDRUJUK" id="asal' . $ds['KODE'] . '" ' . $sel . ' value="' . $ds['KODE'] . '">
										' . $ds['NAMA'] . '
									</label>
								</div>';
                }

                $css = 'style="display:none; float:left;"';
                if ($_GET['KETRUJUK'] != ''):
                    $css = 'style="display:inline;"';
                endif;
                ?>
            </div>
        </div>

    </fieldset>

    <div id="all" style="margin-bottom: 20px">	
        <? include("include/view_prosess.php");?>
    </div>
<?php genBodyCardEnd(); ?>
    <div class="card-footer">
		<div class="d-flex justify-content-between align-items-center">
			<button type="button"  onClick="history.go(0)"  class="btn btn-light">Batal</button>
			<div class="d-inline-flex">
				<button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-left"><b><i class="icon-floppy-disk"></i></b> Simpan</button>
				<button type="button" class="btn bg-slate btn-labeled btn-labeled-left ml-1" onclick="cetak()"><b><i class="icon-printer2"></i></b>Print Kartu Pasien</button>
			</div>
			
		</div>
	</div>
</form>



<script src="assets/js/global.js"></script>
