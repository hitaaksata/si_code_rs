<SCRIPT LANGUAGE="JavaScript">
/*
function stopjam(){
var d = new Date();
var curr_hour = d.getHours();
var curr_min = d.getMinutes();
var curr_sec = d.getSeconds();
document.getElementById('stop_daftar').value=(curr_hour + ":" + curr_min+ ":" + curr_sec);
}
*/




$(document).ready(function(){
  $("#TGLLAHIR").blur(function(){
    calage1($(this).val(),'umur');
  });
  <? if(!empty($_GET['TGLLAHIR'])){ ?>    
    calage1('<?=$_GET['TGLLAHIR']?>','umur');
  <? } ?>

});
  

</SCRIPT>


<fieldset class="mb-3">
  <legend class="text-uppercase font-weight-bold">DATA PASIEN</legend>

  <div class="form-group row">
    <?php genLabel("Nama Lengkap Pasien"); ?>
    <div class="col-lg-4">
      <?php 
        $nama = (!empty($_GET['NAMA']))?$_GET['NAMA']:"";
        genInputText("NAMA","NAMA",$nama,"text","","required");
      ?>
    </div>
    <div class="col-lg-2">
      <select class="form-control form-control-sm" name="CALLER" id="CALLER">
          <option selected="selected" value="">- Alias -</option>
          <option value="Tn" <? if($_GET['CALLER']=="Tn") echo "selected=selected"; ?>> Tn </option>
          <option value="Ny" <? if($_GET['CALLER']=="Ny") echo "selected=selected"; ?>> Ny </option>
          <option value="Nn" <? if($_GET['CALLER']=="Nn") echo "selected=selected"; ?>> Nn </option>
          <option value="An" <? if($_GET['CALLER']=="An") echo "selected=selected"; ?>> An </option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <?php genLabel("Tempat/Tgl Lahir"); ?>
    <div class="col-lg-2">
      <?php 
        $TEMPAT = (!empty($_GET['TEMPAT']))?$_GET['TEMPAT']:$m_pasien->TEMPAT;
        genInputText("TEMPAT","TEMPAT",$TEMPAT,"text","","required"); 
      ?>
    </div>
    <div class="col-lg-2">
      <?php 
        $TGLLahir =(!empty($_GET['TGLLAHIR']))?$_GET['TGLLAHIR']:date('Y-m-d', strtotime($m_pasien->TGLLAHIR));
        genInputText("TGLLAHIR","TGLLAHIR",$TGLLahir, "date","","required");
      ?>
    </div>
  </div>

  <div class="form-group row">
    <?php genLabel("Umur") ?>
    <div class="col-lg-3">
      <?php 
        if ($m_pasien->TGLLAHIR==""){
          $a = datediff(date("Y/m/d"), date("Y/m/d"));
        }
        else {
           $a = datediff($m_pasien->TGLLAHIR, date("Y/m/d"));
        }
        $val_umur = 'umur '.$a[years].' tahun '.$a[months].' bulan '.$a[days].' hari';
        genInputText("umur","umur",$val_umur);
      ?>
    </div>
  </div>

    <div class="form-group row">
      <?php genLabel("Alamat Sekarang");  ?>
      <div class="col-lg-8">
        <?php 
          $alamatSekarang = (!empty($_GET['ALAMAT']))?$_GET['ALAMAT']:"";
          genInputText("ALAMAT","ALAMAT",$alamatSekarang,"text","","required");
        ?>
      </div>
    </div>

    <div class="form-group row">
      <?php genLabel("Alamat KTP");  ?>
      <div class="col-lg-8">
        <?php 
          $alamatktp = (!empty($_GET['ALAMAT_KTP']))?$_GET['ALAMAT_KTP']:"";
          genInputText("ALAMAT_KTP","ALAMAT_KTP",$alamatktp);
        ?>
      </div>
    </div>
  
    <div class="form-group row">
        <?php genLabel("Provinsi");?>
        <div class="col-lg-4">
            <select class="form-control form-control-sm select2" name="KDPROVINSI" id="KDPROVINSI" required>
              <option value="">-Pilih-</option>
              <?php
                  $ss	= mysql_query('select * from m_provinsi order by idprovinsi ASC');
                  while($ds = mysql_fetch_array($ss)){
                        if($_GET['KDPROVINSI'] == $ds['idprovinsi']): $sel = "selected=Selected"; else: $sel = ''; endif;
                        echo '<option value="'.$ds['idprovinsi'].'" '.$sel.' > '.$ds['namaprovinsi'].'</option>';
                  }
              ?>
            </select>
            <?php 
              genInputText("KOTAHIDDEN", "KOTAHIDDEN", $_GET['kota'],"hidden"); 
              genInputText("KECAMATANHIDDEN", "KECAMATANHIDDEN", $_GET['KECAMATAN'],"hidden"); 
              genInputText("KELURAHANHIDDEN", "KELURAHANHIDDEN", $_GET['KELURAHAN'],"hidden"); 
             ?>
        </div>
    </div>
  
    <div class="form-group row">
        <?php genLabel("Kabupaten / Kota") ?>
        <div class="col-lg-4">
          <div id="kotapilih">
            <select class="form-control form-control-sm select2" name="KOTA" id="KOTA" required>
                <option value="">-Pilih-</option>
                <?php
                  $ss	= mysql_query('select * from m_kota where idprovinsi = "'.$_GET['KDPROVINSI'].'" order by idkota ASC');
                  while($ds = mysql_fetch_array($ss)){
                        if($_GET['KOTA'] == $ds['idkota']): $sel = "selected=Selected"; else: $sel = ''; endif;
                        echo '<option value="'.$ds['idkota'].'" '.$sel.' > '.$ds['namakota'].'</option>';
                  }
                ?>
            </select>
          </div>
        </div>
    </div>
  
    <div class="form-group row">
      <?php genLabel("Kecamatan") ?>
      <div class="col-lg-4">
        <div id="kecamatanpilih">
          <select class="form-control form-control-sm select2" name="KDKECAMATAN" id="KDKECAMATAN" required>
              <option value="">-Pilih-</option>
              <?php
                $ss	= mysql_query('select * from m_kecamatan where idkota = "'.$_GET['KOTA'].'" order by idkecamatan ASC');
                while($ds = mysql_fetch_array($ss)){
                      if($_GET['KDKECAMATAN'] == $ds['idkecamatan']): $sel = "selected=Selected"; else: $sel = ''; endif;
                      echo '<option value="'.$ds['idkecamatan'].'" '.$sel.' /> '.$ds['namakecamatan'].'</option>&nbsp;';
                }
              ?>
          </select>
        </div>
      </div>
    </div>
  
    <div class="form-group row">
      <?php genLabel("Kelurahan") ?>
      <div class="col-lg-4">
        <div id="kelurahanpilih">
          <select class="form-control form-control-sm select2" name="KELURAHAN" id="KELURAHAN" required>
              <option value="">-Pilih-</option>
              <?php
                $ss	= mysql_query('select * from m_kelurahan where idkecamatan = "'.$_GET['KDKECAMATAN'].'" order by idkelurahan ASC');
                while($ds = mysql_fetch_array($ss)){
                      if($_GET['KELURAHAN'] == $ds['idkelurahan']): $sel = "selected=Selected"; else: $sel = ''; endif;
                      echo '<option value="'.$ds['idkelurahan'].'" '.$sel.' /> '.$ds['namakelurahan'].'</option>&nbsp;';
                }
              ?>
          </select>
        </div>
      </div>
    </div>
  <div class="form-group row">
      <?php     genLabel("No. telepon / HP") ?>
      <div class="col-lg-3">
          <?php 
            $notelp = (!empty($_GET['NOTELP']))? $_GET['NOTELP']:$m_pasien->NOTELP;
            genInputText("NOTELP", "notelp", $notelp); 
           ?>
      </div>
  </div>
  
  <div class="form-group row">
      <?php genLabel("No. KTP") ?>
      <div class="col-lg-3">
          <?php 
                      $noKTP = (!empty($_GET['NOKTP']))?$_GET['NOKTP']:$m_pasien->NOKTP;
                     genInputText("NOKTP", "NOKTP", $noKTP);
          ?>
      </div>
  </div>
  
  <div class="form-group row">
      <?php genLabel("Jenis Kelamin") ?>
      <div class="col-lg-8">
            <?php 
                  
                  if(strtoupper($_GET['JENISKELAMIN'])=="L"){
                      $JK_L = "checked";
                      $JK_P ="";
                  }elseif (strtoupper($_GET['JENISKELAMIN'])=="P") {
                      $JK_L = "";
                      $JK_P="checked";
                  }
            ?>
          <div class="form-check form-check-inline">
              <label class="form-check-label">
                
                  <input type="radio"  name="JENISKELAMIN" id="JENISKELAMIN_L" value="L" class="form-check-input" <?= $JK_L ?> required>
                  Laki - Laki
              </label>
          </div>
          <div class="form-check form-check-inline">
              <label class="form-check-label">
                   <input type="radio"  name="JENISKELAMIN" id="JENISKELAMIN_P" value="P" class="form-check-input" <?= $JK_P ?> required>
                  Perempuan
              </label>
          </div>
      </div>
  </div>
  
  <div class="form-group row">
    <?php genLabel(" Status Perkawinan") ?>
    <div class="col-lg-8">
      <div class="form-check form-check-inline">
          <label class="form-check-label">
             <input type="radio" name="STATUS" id="status_1" class="form-check-input" value="1" <? if($m_pasien->STATUS=="1" || $_GET['STATUS']=="1"){echo "Checked";}?>/>
            Belum Menikah
          </label>
      </div>
      <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input type="radio" name="STATUS" id="status_2" value="2" class="form-check-input" <? if($m_pasien->STATUS=="2" || $_GET['STATUS']=="2") echo "Checked";?> />
             Menikah
          </label>
      </div>
      <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input type="radio" name="STATUS" id="status_3" value="3" class="form-check-input" <? if($m_pasien->STATUS=="3" || $_GET['STATUS']=="3") echo "Checked";?>/>
              Janda / Duda
          </label>
      </div>
    </div>
  </div>

  <div class="form-group row">
    <?php genLabel("Pendidikan Terakhir") ?>
    <div class="col-lg-8">
      <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_1" value="1" class="form-check-input" <? if($m_pasien->PENDIDIKAN=="1" || $_GET['PENDIDIKAN']=="1"){echo "Checked";}?> />
            SD
          </label>
      </div>
      <div class="form-check form-check-inline">
          <label class="form-check-label">
             <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_2" value="2" class="form-check-input" <? if($m_pasien->PENDIDIKAN=="2" || $_GET['PENDIDIKAN']=="2") echo "Checked";?> />
            SLTP
          </label>
      </div>
      <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_3" value="3" class="form-check-input" <? if($m_pasien->PENDIDIKAN=="3" || $_GET['PENDIDIKAN']=="3") echo "Checked";?> />
            SMU / SMK
          </label>
      </div>
      <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_5" value="5" class="form-check-input" <? if($m_pasien->PENDIDIKAN=="5" || $_GET['PENDIDIKAN']=="5") echo "Checked";?> />
            Universitas
          </label>
      </div>
    </div>
  </div>

  <div class="form-group row">
    <?php genLabel("Agama") ?>
    <div class="col-lg-8">
      <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input type="radio" name="AGAMA" id="AGAMA_1" value="1" class="form-check-input" <? if($m_pasien->AGAMA=="1" || $_GET['AGAMA']=="1") echo "Checked";?> />
            Islam
          </label>
      </div>
      <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input type="radio" name="AGAMA" id="AGAMA_2" value="2" class="form-check-input" <? if($m_pasien->AGAMA=="2" || $_GET['AGAMA']=="2") echo "Checked";?>/>
            Kristen Protestan
          </label>
      </div>
      <div class="form-check form-check-inline">
          <label class="form-check-label">
           <input type="radio" name="AGAMA" id="AGAMA_3" value="3" class="form-check-input" <? if($m_pasien->AGAMA=="3" || $_GET['AGAMA']=="3"){echo "Checked";}?>/>
            Katholik
          </label>
      </div>
      <div class="form-check form-check-inline">
          <label class="form-check-label">
           <input type="radio" name="AGAMA" id="AGAMA_4" value="4" class="form-check-input" <? if($m_pasien->AGAMA=="4" || $_GET['AGAMA']=="4") echo "Checked";?>/>
            Hindu
          </label>
      </div>
      <div class="form-check form-check-inline">
          <label class="form-check-label">
           <input type="radio" name="AGAMA" id="AGAMA_5" value="5" class="form-check-input" <? if($m_pasien->AGAMA=="5" || $_GET['AGAMA']=="5") echo "Checked";?>/>
            Budha 
          </label>
      </div>
    </div>
  </div>
</fieldset>
  
<fieldset class="mb-3">
  <legend class="text-uppercase font-weight-bold">DATA PENDUKUNG</legend>
  <div class="form-group row">
      <?php genLabel("Nama Suami / Orang Tua ") ?>
      <div class="col-lg-4">
          <?php 
                      $nosuami_ortu = (!empty($_GET['SUAMI_ORTU']))?$_GET['SUAMI_ORTU']:$m_pasien->SUAMI_ORTU;
                     genInputText("SUAMI_ORTU", "SUAMI_ORTU", $nosuami_ortu);
          ?>
      </div>
  </div>
  
   <div class="form-group row">
      <?php genLabel("Pekerjaan Pasien / Orang Tua ") ?>
      <div class="col-lg-2">
          <?php 
                      $pekerjaan = (!empty($_GET['PEKERJAAN']))? $_GET['PEKERJAAN']:$m_pasien->PEKERJAAN;
                     genInputText("PEKERJAAN", "PEKERJAAN", $pekerjaan);
          ?>
      </div>
  </div>
  
  <div class="form-group row">
      <?php genLabel("Nama Penanggung Jawab ") ?>
      <div class="col-lg-4">
          <?php 
                      $nama_penanggungjawab =(!empty($_GET['nama_penanggungjawab']))?$_GET['nama_penanggungjawab']:""; 
                     genInputText("nama_penanggungjawab", "nama_penanggungjawab", $nama_penanggungjawab);
          ?>
      </div>
  </div>
  
  <div class="form-group row">
      <?php genLabel("Hubungan Dengan Pasien ") ?>
      <div class="col-lg-2">
          <?php 
                     $hubungan_penanggungjawab =(!empty($_GET['hubungan_penanggungjawab']))?$_GET['hubungan_penanggungjawab']:""; 
                     genInputText("hubungan_penanggungjawab", "hubungan_penanggungjawab", $hubungan_penanggungjawab);
          ?>
      </div>
  </div>
  
  <div class="form-group row">
      <?php genLabel("Alamat ") ?>
      <div class="col-lg-8">
          <?php 
                     $alamat_penanggungjawab =(!empty($_GET['alamat_penanggungjawab']))?$_GET['alamat_penanggungjawab']:""; 
                     genInputText("alamat_penanggungjawab", "alamat_penanggungjawab", $alamat_penanggungjawab);
          ?>
      </div>
  </div>
  
    <div class="form-group row">
      <?php genLabel("No Telepon / HP ") ?>
      <div class="col-lg-3">
          <?php 
                     $phone_penanggungjawab =(!empty($_GET['phone_penanggungjawab']))?$_GET['phone_penanggungjawab']:""; 
                     genInputText("phone_penanggungjawab", "phone_penanggungjawab", $phone_penanggungjawab);
          ?>
      </div>
  </div>
</fieldset>


<script language="javascript" type="text/javascript">
  function cetak(){
    var nomr = document.getElementById('NOMR').value;
    var nama = document.getElementById('NAMA').value;
    var alamat = document.getElementById('ALAMAT').value;
    window.open("komponen/pdfb/kartupasien.php?NOMR="+ nomr +"&NAMA="+ nama +"&ALAMAT="+ alamat,"mywindow");
    return false;
  }
</script>