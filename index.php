<?php
session_start();

if (!isset($_SESSION['SES_REG'])) {
    header("location:login.php");
}
include("include/connect.php");
include("include/function.php");
include('include/phpMyBorder2.class.php');
$pmb = new PhpMyBorder(false);

if (isset($_GET["link"])) {
    $link = $_GET["link"];
} else {
    $link = "";
}
//echo $link;
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?= ucwords($rstitle) ?></title>
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/forms/validation/validate.min.js"></script>
        <script src="global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/select2.min.js"></script>

        <script src="assets/js/app.js"></script>



        <!-- /theme JS files -->
    </head>
    <body>

        <!-- Header Konten -->
        <div class="navbar navbar-expand-md navbar-dark">
            <div class="navbar-brand">
                <a href="" class="d-inline-block">
                    <img src="global_assets/images/logo_light.png" alt="">
                </a>
            </div>

            <div class="d-md-none">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                    <i class="icon-tree5"></i>
                </button>
                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                    <i class="icon-paragraph-justify3"></i>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                            <i class="icon-paragraph-justify3"></i>
                        </a>
                    </li>
                </ul>

                <span class="navbar-text ml-md-3 mr-md-auto">
                </span>

                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                            <i class="icon-bubbles4"></i>
                            <span class="d-md-none ml-2">Messages</span>
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0">2</span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold">Messages</span>
                                <a href="#" class="text-default"><i class="icon-compose"></i></a>
                            </div>

                            <div class="dropdown-content-body dropdown-scrollable">
                                <ul class="media-list">
                                    <li class="media">
                                        <div class="mr-3 position-relative">
                                            <img src="global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
                                        </div>

                                        <div class="media-body">
                                            <div class="media-title">
                                                <a href="#">
                                                    <span class="font-weight-semibold">James Alexander</span>
                                                    <span class="text-muted float-right font-size-sm">04:58</span>
                                                </a>
                                            </div>

                                            <span class="text-muted">who knows, maybe that would be the best thing for me...</span>
                                        </div>
                                    </li>

                                    <li class="media">
                                        <div class="mr-3 position-relative">
                                            <img src="global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
                                        </div>

                                        <div class="media-body">
                                            <div class="media-title">
                                                <a href="#">
                                                    <span class="font-weight-semibold">Margo Baker</span>
                                                    <span class="text-muted float-right font-size-sm">12:16</span>
                                                </a>
                                            </div>

                                            <span class="text-muted">That was something he was unable to do because...</span>
                                        </div>
                                    </li>

                                    <li class="media">
                                        <div class="mr-3">
                                            <img src="global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
                                        </div>
                                        <div class="media-body">
                                            <div class="media-title">
                                                <a href="#">
                                                    <span class="font-weight-semibold">Jeremy Victorino</span>
                                                    <span class="text-muted float-right font-size-sm">22:48</span>
                                                </a>
                                            </div>

                                            <span class="text-muted">But that would be extremely strained and suspicious...</span>
                                        </div>
                                    </li>

                                    <li class="media">
                                        <div class="mr-3">
                                            <img src="global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
                                        </div>
                                        <div class="media-body">
                                            <div class="media-title">
                                                <a href="#">
                                                    <span class="font-weight-semibold">Beatrix Diaz</span>
                                                    <span class="text-muted float-right font-size-sm">Tue</span>
                                                </a>
                                            </div>

                                            <span class="text-muted">What a strenuous career it is that I've chosen...</span>
                                        </div>
                                    </li>

                                    <li class="media">
                                        <div class="mr-3">
                                            <img src="global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
                                        </div>
                                        <div class="media-body">
                                            <div class="media-title">
                                                <a href="#">
                                                    <span class="font-weight-semibold">Richard Vango</span>
                                                    <span class="text-muted float-right font-size-sm">Mon</span>
                                                </a>
                                            </div>

                                            <span class="text-muted">Other travelling salesmen live a life of luxury...</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="dropdown-content-footer justify-content-center p-0">
                                <a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="Load more"><i class="icon-menu7 d-block top-0"></i></a>
                            </div>
                        </div>
                    </li>

                    <li class="nav-item dropdown dropdown-user">
                        <?php
                        $dep = "SELECT * FROM m_login WHERE KDUNIT = '" . $_SESSION['KDUNIT'] . "' AND ROLES = '" . $_SESSION['ROLES'] . "'";
                        $qe = mysql_query($dep);
                        if ($qe) {
                            $deps = mysql_fetch_assoc($qe);
                        }
                        ?>
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                            <img src="global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" alt="">
                            <span><?= $deps['DEPARTEMEN']; ?></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                            <a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
                            <a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
                            <a href="log_out.php" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /End Heade Konten -->

        <!-- Page Konten -->
        <div class="page-content">

            <!-- SideBar -->
            <div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

                <!-- SideBar Mobile Toggler -->
                <div class="sidebar-mobile-toggler text-center">
                    <a href="#" class="sidebar-mobile-main-toggle">
                        <i class="icon-arrow-left8"></i>
                    </a>
                    Navigasi
                    <a href="#" class="sidebar-mobile-expand">
                        <i class="icon-screen-full"></i>
                        <i class="icon-screen-normal"></i>
                    </a>
                </div>
                <!-- /End SideBar Mobile Toggler -->


                <!-- SideBar Content -->
                <div class="sidebar-content">

                    <!-- User Menu -->
                    <div class="sidebar-user">
                        <div class="card-body">
                            <div class="media">
                                <div class="mr-3">
                                    <a href="#"><img src="global_assets/images/placeholders/placeholder.jpg" width="38" height="38" class="rounded-circle" alt=""></a>
                                </div>

                                <div class="media-body">
                                    <div class="media-title font-weight-semibold"><?= $deps['DEPARTEMEN']; ?></div>
                                    <div class="font-size-xs opacity-50">
                                        <i class="icon-pin font-size-sm"></i> &nbsp;BLUD
                                    </div>
                                </div>

                                <div class="ml-3 align-self-center">
                                    <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /End User Menu -->


                    <!-- Menu Navigation -->
                    <div class="card card-sidebar-mobile">
                        <ul class="nav nav-sidebar" data-type="accordian">
                            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                            <?php include("komponen/menu.php") ?>
                        </ul>
                    </div>
                    <!-- /End Menu Navigation -->				

                </div>
                <!-- /End SideBar Content -->	

            </div>
            <!-- /End SideBar -->


            <!-- main content -->
            <div class="content-wrapper">
                <div class="page-header page-header-light">
                    <div class="page-header-content header-elements-md-inline">
                        <div class="page-title d-flex">
                            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Forms</span> - Basic Inputs</h4>
                            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                        </div>

                        <div class="header-elements d-none">
                            <div class="d-flex justify-content-center">
                                <a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                                <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                                <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- Content Page -->
                <div class="content">
                    <?php include("komponen/genForm.php") ?>
                    <?php include("komponen/routes.php") ?>
                </div>
                <!-- /End Content Page -->

                <!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</span>

					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
						<li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>
						<li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
					</ul>
				</div>
			</div>
			<!-- /footer -->

            </div>
            <!-- /End main content -->		

        </div>
        <!-- /End Page Konten -->
        <script src="assets/js/global.js"></script>
        <script type="text/javascript">

        	$(document).on("change","#ID_DIAGNOSIS",function(){
    var selectValues = $("#ID_DIAGNOSIS").val();
    //var kecHidden = $("#KECAMATANHIDDEN").val();
    $.post('./include/ajaxload.php',{id_diagnosis:selectValues,load_sub_var1:'true'},function(data){
      $('#dafgejalapilih').html(data);
    });
    $.post('./include/ajaxload.php',{id_diagnosis:selectValues,load_sub_var2:'true'},function(data){
      $('#dafetiologipilih').html(data);
    });
});
        </script>
    </body>
</html>