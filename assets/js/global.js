var startyear = "1950";
var endyear = "2010";
var dat = new Date();
var curday = dat.getDate();
var curmon = dat.getMonth()+1;
var curyear = dat.getFullYear();

function DaysInMonth(Y, M) {
    with (new Date(Y, M, 1, 12)) {
        setDate(0);
        return getDate();
    }
}

function datediff(date1, date2) {
    var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
     y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();

    if (d1 < d2) {
        m1--;
        d1 += DaysInMonth(y2, m2);
    }
    if (m1 < m2) {
        y1--;
        m1 += 12;
    }
    return [y1 - y2, m1 - m2, d1 - d2];
}
function calage1(ctgl, objID)
{
    //alert(ctgl);
    var calday = ctgl.substr(8,2);
    var calmon = ctgl.substr(5,2);
    var calyear = ctgl.substr(0,4);

    //alert(calday+" "+calmon+" "+calyear);
    if(curday == "" || curmon=="" || curyear=="" || calday=="" || calmon=="" || calyear=="")
        {
        //alert("please fill all the values and click go -");
        }   
    else
        {
            var curd = new Date(curyear,curmon-1,curday);
            var cald = new Date(calyear,calmon-1,calday);
            
            var diff =  Date.UTC(curyear,curmon,curday,0,0,0) - Date.UTC(calyear,calmon,calday,0,0,0);

            var dife = datediff(curd,cald);
            vobj = document.getElementById(objID);
            vobj.value = dife[0]+" tahun "+dife[1]+" bulan "+dife[2]+" hari";
        }

}

function startjam(){
        var d = new Date();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();
        document.getElementById('start_daftar').value=(curr_hour + ":" + curr_min+ ":" + curr_sec);
}

$(document).off("change", "#KOTA");
$(document).off("change", "#KDKECAMATAN");
$(document).off("change", "#KELURAHAN");
$(document).off("change", "#KDPROVINSI");

$(document).ready(function(){  
    $("#KDPROVINSI").change(function(){
        var selectValues = $("#KDPROVINSI").val();
        var kotaHidden = $("#KOTAHIDDEN").val();
        var kecHidden = $("#KECAMATANHIDDEN").val();
        $.post('./include/ajaxload.php',{kdprov:selectValues, kdkota:kotaHidden, kdkec:kecHidden, load_kota:'true'},function(data){
            $('#kotapilih').html(data);
            //alert("tes");
            //$('#KOTA').val(kotaHidden);
            //$('#KOTA').val(kotaHidden).change();
            if($.trim(kecHidden)!=""|| $.trim(kecHidden)!=0){
             $('#KOTA').val(kotaHidden).change();
             
            }else{
                 $("#KOTA").prop('selectedIndex', 0);
                  
            }
            //$('#KOTA').select2().trigger('change');
            $('#kecamatanpilih').html("<select name=\"KDKECAMATAN\" class=\"form-control form-control-sm select2\" title=\"*\" id=\"KDKECAMATAN\"><option value=\"0\"> --pilih-- </option></select>");
            $('#kelurahanpilih').html("<select name=\"KELURAHAN\" class=\"form-control form-control-sm select2\" title=\"*\" id=\"KELURAHAN\"><option value=\"0\"> --pilih-- </option></select>");
            $(".select2").select2();
            /*$("KOTAHIDDEN").val("");
            $("KECAMATANHIDDEN").val("");
            $("KELURAHANHIDDEN").val("");*/
        });
    });
});



$(document).on("change","#KOTA",function(){
   var selectValues = $("#KOTA").val();
    var kecHidden = $("#KECAMATANHIDDEN").val();
    $.post('./include/ajaxload.php',{kdkota:selectValues,load_kecamatan:'true'},function(data){
      $('#kecamatanpilih').html(data);
      //$('#KDKECAMATAN').val(kecHidden).change();
       if($.trim(kecHidden)!=""|| $.trim(kecHidden)!=0){
         $('#KDKECAMATAN').val(kecHidden).change();
          
        }else{
             $("#KDKECAMATAN").prop('selectedIndex', 0);
              
        }
      $('#kelurahanpilih').html("<select name=\"KELURAHAN\" class=\"form-control form-control-sm select2\" title=\"*\" id=\"KELURAHAN\"><option value=\"0\"> --pilih-- </option></select>");
        
       
       
      $(".select2").select2();
            
    });
});


$(document).on("change","#KDKECAMATAN",function(){
    var selectValues = $("#KDKECAMATAN").val();
    var kelHidden = $("#KELURAHANHIDDEN").val();
    $.post('./include/ajaxload.php',{kdkecamatan:selectValues,load_kelurahan:'true'},function(data){
      $('#kelurahanpilih').html(data);
      //alert(kelHidden);
      if($.trim(kelHidden)!="" || $.trim(kelHidden)!=0){
         $('#KELURAHAN').val(kelHidden).change();
         
      }else{
         $("#KELURAHAN").prop('selectedIndex', 0);
          
      }
     
      $(".select2").select2();
       $("KOTAHIDDEN").val("");
       $("KECAMATANHIDDEN").val("");
        $("KELURAHANHIDDEN").val("");
        $("#KELURAHANHIDDEN").val("");
       
    });
});

$(document).ready(function(){
    $.validator.messages.required = "Tidak Boleh Kosong / Pilih salah satu";
    $(".select2").select2();
});