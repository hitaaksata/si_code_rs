<!DOCTYPE html>
<html>
<head>
	<title>Print Data</title>
	<link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/print.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php 
	include("../komponen/inc_kop.php");	
	include("../include/connect.php");
	kopcetak();
	$link = (isset($_GET['link']))?$_GET['link']:"";
	switch ($link) {
		default:

			break;
		case "list_data_pasien":
			include("../view/cetak/c_list_data_pasien.php");
			break;
	}

?>
	<script>
  		window.print();
		setInterval(function(){ window.close(); }, 1000);
	</script>
</body>
</html>
