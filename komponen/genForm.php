<?php 
	function genHeader($title){
		$html ='
		<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Forms</span> - $title</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>

			</div>
			';
		echo $html;
	}

	function genStartCard($title){
		$html ='
			<div class="card border-left-2 border-left-blue rounded-0">
				<div class="card-header bg-white ">
					<h5>'.$title.'</h5>
				</div>
				
		';
		echo $html;
	}

	function genBodyCard(){
		$html = '<div class="card-body">';
		echo $html;
	}
	function genBodyCardEnd(){
		$html = '</div>';
		echo $html;
	}

	function genEndCard(){
		$html = '
			
		</div>
		';
		echo $html;
	}

	function genLabel($text, $class = "col-form-label col-lg-2"){
		echo '<label class="'.$class.'">'.$text.'</label>';
	}

	function genInputText($name, $id, $val, $type="text", $class = "",  $data = ""){
		echo '<input type="'.$type.'" name="'.$name.'" id="'.$id.'" value="'.$val.'" class="form-control form-control-sm '.$class.'" '.$data.'>';
	}
        
    function genInputRadio($name, $id, $val, $type="radio", $class = "form-check-input",  $data = array(), $placeholder = ""){

		echo '<input type="'.$type.'"  name="'.$name.'" id="'.$id.'" class="'.$class.'" placeholder="'.$placeholder.'" '.$data.'>';
	}


?>