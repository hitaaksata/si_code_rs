<?php
if(!isset($_SESSION['SES_REG'])){
    header("location:login.php");
}

?>

<?php if($_SESSION['ROLES']=="1017") { ?>
	<li class="nav-item nav-item-submenu">
		<a href="#" class="nav-link"><i class="icon-copy"></i> <span>MASTER</span></a>
		<ul class="nav nav-group-sub" data-submenu-title="Layouts">
			<li class="nav-item"><a class="nav-link" href="index.php?link=add_user">ADD USER</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?link=private">LIST USER</a></li>
            <li class="nav-item"><a class="nav-link" href="?link=191">EDIT ICD</a></li>
            <li class="nav-item"><a class="nav-link" href="?link=19">LIST ICD</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?link=jdoc2">ADD JADWAL</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?link=jdoc3">LIST JADWAL</a></li>
		</ul>
	</li>


<?php }elseif($_SESSION['ROLES']=="1") { ?>
 	<li class="nav-item nav-item-submenu">
		<a href="#" class="nav-link"><i class="icon-copy"></i> <span>REGISTRASI</span></a>
		<ul class="nav nav-group-sub" data-submenu-title="Layouts">
			<li class="nav-item"><a class="nav-link" href="index.php?link=2">Reg Pasien</a></li>
			<li class="nav-item"><a class="nav-link" href="index.php?link=telepon">Reg melalui Telepon</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?link=2bayi">Reg Bayi Baru Lahir</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?link=21">List Pasien</a></li>
		    <li class="nav-item"><a class="nav-link" href="index.php?link=22">List Kunjungan</a></li>
		    <li class="nav-item"><a class="nav-link" href="index.php?link=14_">Asuransi</a></li>
		    <li class="nav-item"><a class="nav-link" href="index.php?link=140">Rekap Pendf. Pasien RJ</a></li>
            <li class="nav-item"><a class="nav-link" href="?link=2f" >Data Asuransi</a></li>
		</ul>
	</li>


<?php }elseif($_SESSION['ROLES']=="2") { ?>
	<li class="nav-item nav-item-submenu">
		<a href="#" class="nav-link"><i class="icon-copy"></i> <span>REGISTRASI</span></a>
		<ul class="nav nav-group-sub" data-submenu-title="Layouts">
			<li class="nav-item"><a class="nav-link" href="index.php?link=2">Reg Pasien</a></li>
			<li class="nav-item"><a class="nav-link" href="index.php?link=telepon">PENDAFTARAN MELALUI TELEPON</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?link=2bayi">PENDAFTARAN BAYI BARU LAHIR</a></li>
            <li class="nav-item"><a class="nav-link" href="index.php?link=21">LIST DATA PASIEN</a></li>
		    <li class="nav-item"><a class="nav-link" href="index.php?link=22">LIST KUNJUNGAN PASIEN</a></li>
		    <li class="nav-item"><a class="nav-link" href="index.php?link=14_">ASURANSI</a></li>
		    <li class="nav-item"><a class="nav-link" href="index.php?link=140">REKAP PENDAFTARAN PASIEN RAWAT JALAN</a></li>
            <li class="nav-item"><a class="nav-link" href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
		</ul>
	</li>



<?php } ?>