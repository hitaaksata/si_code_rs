<?php
error_reporting (0);  
require('pdfb/pdfb.php');

class PDF extends PDFB {
    function Header() {
    }

    function Footer() {
        //$this->Text(402, 735, "Dynamic PDF: PDFB Library!");
    }

 }

// Create a PDF object and set up the properties
$pdf = new PDF("p", "mm", array(85,60));
$pdf->SetAuthor("PDFB Library");
$pdf->SetTitle("Kartu Pasien");

$pdf->SetFont("Arial", "", 15);
$pdf->SetAutoPageBreak(false);

// Load the base PDF into template
$pdf->setSourceFile("kartu_pasien.pdf");
$tplidx = $pdf->ImportPage(1);

// Add new page & use the base PDF as template
$pdf->AddPage();
$pdf->useTemplate($tplidx);

// Probably load Packing Slip information from a database.
//$pkgslip = rand(1000,9999);


//$pdf->SetLeftMargin(20);
//$pdf->SetRightMargin(15);
//$pdf->Text(5, 28.5, $_GET['NOMR']);
//$pdf->Text(5, 36.5, $_GET['NAMA']);

$pdf->Text(5, 33.5, $_GET['NOMR']);
$pdf->Text(5, 42.5, $_GET['NAMA']);
$pdf->BarCode($_GET['NOMR'], "C128B", 70, 49, 15, 5, 1, 1, 2, 5, "", "PNG");
$pdf->SetXY(32.18,28);
//$pdf->MultiCell(55,5,$_GET['ALAMAT'],0,1,'');

$pdf->Output();
$pdf->closeParsers();
?>