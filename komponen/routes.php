<?php
#session_start();
switch ($link) {
    default			:
        if (!file_exists ("view/main.php"))
            die ("main.php File Empty!");
        include 'viewmain.php';
        break;
    //grafik dashboard
    case 'private'            :
        if (!file_exists ("view/adm/list_data_user.php"))
            die ("adm/list_data_user.php File Empty!");
        include 'view/adm/list_data_user.php';
        break;
    case 'private2'            :
        if (!file_exists ("view/main.php"))
            die ("main.php File Empty!");
        include 'view/main.php';
        break;
    case 'private21'            :
        if (!file_exists ("view/adm/eksekutif/slide/index.php"))
            die ("adm/eksekutif/slide/index.php File Empty!");
        include 'view/adm/eksekutif/slide/index.php';
        break;
    case 'private22'            :
        if (!file_exists ("view/adm/eksekutif/slide/index_rujukan.php"))
            die ("adm/eksekutif/slide/index_rujukan.php File Empty!");
        include 'view/adm/eksekutif/slide/index_rujukan.php';
        break;
    case 'private23'            :
        if (!file_exists ("view/adm/eksekutif/slide/index_carabayar.php"))
            die ("adm/eksekutif/slide/index_carabayar.php File Empty!");
        include 'view/adm/eksekutif/slide/index_carabayar.php';
        break;
    case 'private24'            :
        if (!file_exists ("view/adm/eksekutif/slide/index_10penyakit.php"))
            die ("adm/eksekutif/slide/index_10penyakit.php File Empty!");
        include 'view/adm/eksekutif/slide/index_10penyakit.php';
        break;
    case 'private25'            :
        if (!file_exists ("view/adm/eksekutif/slide/index_pendapatan.php"))
            die ("adm/eksekutif/slide/index_pendapatan.php File Empty!");
        include 'view/adm/eksekutif/slide/index_pendapatan.php';
        break;
    case 'private26'            :
        if (!file_exists ("view/adm/eksekutif/slide/index_ranap.php"))
            die ("adm/eksekutif/slide/index_ranap.php File Empty!");
        include 'view/adm/eksekutif/slide/index_ranap.php';
        break;
	case 'private26_crbyr'            :
        if (!file_exists ("view/adm/eksekutif/slide/pendapatan_ranap_percarabayar.php"))
            die ("adm/eksekutif/slide/pendapatan_ranap_percarabayar.php File Empty!");
        include 'view/adm/eksekutif/slide/pendapatan_ranap_percarabayar.php';
        break;
	case 'private27'            :
        if (!file_exists ("view/adm/eksekutif/slide/pendapatan_rajal_percarabayar.php"))
            die ("adm/eksekutif/slide/pendapatan_rajal_percarabayar.php File Empty!");
        include 'view/adm/eksekutif/slide/pendapatan_rajal_percarabayar.php';
        break;
	case 'private27All'            :
        if (!file_exists ("view/adm/eksekutif/slide/total_pendapatan.php"))
            die ("adm/eksekutif/slide/total_pendapatan.php File Empty!");
        include 'view/adm/eksekutif/slide/total_pendapatan.php';
        break;
    case 'privatelab1'            :
        if (!file_exists ("view/adm/eksekutif/slide/lab_index_carabayar.php"))
            die ("adm/eksekutif/slide/lab_index_carabayar.php File Empty!");
        include 'view/adm/eksekutif/slide/lab_index_carabayar.php';
        break;
    case 'privatelab2'            :
        if (!file_exists ("view/adm/eksekutif/slide/lab_index_10periksa.php"))
            die ("adm/eksekutif/slide/lab_index_10periksa.php File Empty!");
        include 'view/adm/eksekutif/slide/lab_index_10periksa.php';
        break;
    case 'privaterad1'            :
        if (!file_exists ("view/adm/eksekutif/slide/rad_index_carabayar.php"))
            die ("adm/eksekutif/slide/rad_index_carabayar.php File Empty!");
        include 'view/adm/eksekutif/slide/rad_index_carabayar.php';
        break;
    case 'privaterad2'            :
        if (!file_exists ("view/adm/eksekutif/slide/rad_index_10periksa.php"))
            die ("adm/eksekutif/slide/rad_index_10periksa.php File Empty!");
        include 'view/adm/eksekutif/slide/rad_index_10periksa.php';
        break;
    case 'privatekam1'            :
        if (!file_exists ("view/adm/eksekutif/slide/kam_index_carabayar.php"))
            die ("adm/eksekutif/slide/kam_index_carabayar.php File Empty!");
        include 'view/adm/eksekutif/slide/kam_index_carabayar.php';
        break;
    case 'privatekam2'            :
        if (!file_exists ("view/adm/eksekutif/slide/kam_index_pembedahan.php"))
            die ("adm/eksekutif/slide/kam_index_pembedahan.php File Empty!");
        include 'view/adm/eksekutif/slide/kam_index_pembedahan.php';
        break;
	
	case 'privateapotek1'            :
        if (!file_exists ("view/adm/eksekutif/slide/apotek_index_carabayar.php"))
            die ("adm/eksekutif/slide/apotek_index_carabayar.php File Empty!");
        include 'view/adm/eksekutif/slide/apotek_index_carabayar.php';
        break;
   case 'privategizi1'            :
        if (!file_exists ("view/adm/eksekutif/slide/gizi_index_carabayar.php"))
            die ("adm/eksekutif/slide/gizi_index_carabayar.php File Empty!");
        include 'view/adm/eksekutif/slide/gizi_index_carabayar.php';
        break;

  case 'pasienrujukan'            :
        if (!file_exists ("view/rm/pasien_rujukan.php"))
            die ("rm/pasien_rujukan.php File Empty!");
        include 'view/rm/pasien_rujukan.php';
        break;

    case 'add_user'            :
        if (!file_exists ("view/adm/form_input_user.php"))
            die ("adm/form_input_user.php File Empty!");
        include 'view/adm/form_input_user.php';
        break;
    case '2'            :
        if (!file_exists ("view/Pendaftaran.php"))
            die ("Pendaftaran.php File Empty!");
        include 'view/Pendaftaran.php';
        break;
	case '2bayi'            :
        if (!file_exists ("view/Pendaftaran_bayi.php"))
            die ("Pendaftaran_bayi.php File Empty!");
        include 'view/view/Pendaftaran_bayi.php';
        break;
    case '2a'            :
        if (!file_exists ("view/PendaftaranKtpDepok.php"))
            die ("PendaftaranKtpDepok.php File Empty!");
        include 'view/PendaftaranKtpDepok.php';
        break;
    case '2b'            :
        if (!file_exists ("view/daftarCariNik.php"))
            die ("daftarCariNik.php File Empty!");
        include 'view/daftarCariNik.php';
        break;
    case '2c'            :
        if (!file_exists ("view/daftarCariNama.php"))
            die ("daftarCariNama.php File Empty!");
        include 'view/daftarCariNama.php';
        break;
    case '2d'            :
        if (!file_exists ("view/PendaftaranKtp.php"))
            die ("PendaftaranKtp.php File Empty!");
        include 'view/PendaftaranKtp.php';
        break;
    case '2f'            :
        if (!file_exists ("view/cari_pasien_asuransi.php"))
            die ("cari_pasien_asuransi.php File Empty!");
        include 'view/cari_pasien_asuransi.php';
        break;
    case '2g'            :
        if (!file_exists ("view/daftarCariAsuransi.php"))
            die ("daftarCariAsuransi.php File Empty!");
        include 'view/daftarCariAsuransi.php';
        break;
    case '21'            :
        if (!file_exists ("view/list_data_pasien.php"))
            die ("list_data_pasien.php File Empty!");
        include 'view/list_data_pasien.php';
        break;
    case '22'            :
        if (!file_exists ("view/list_kunjungan_pasien.php"))
            die ("list_kunjungan_pasien.php File Empty!");
        include 'view/list_kunjungan_pasien.php';
        break;
    case '23'            :
        if (!file_exists ("view/rekap_kunjungan_pasien.php"))
            die ("rekap_kunjungan_pasien.php File Empty!");
        include 'view/rekap_kunjungan_pasien.php';
        break;
    case '24'            :
        if (!file_exists ("view/edit_m_pasien.php"))
            die ("edit_m_pasien.php File Empty!");
        include 'view/edit_m_pasien.php';
        break;
    case '25'            :
        if (!file_exists ("view/include/view_prosess.php"))
            die ("view_prosess.php File Empty!");
        include 'view/include/view_prosess.php';
        break;

    case '26'            :
        if (!file_exists ("view/list_filter_data_pasien.php"))
            die ("list_filter_data_pasien.php File Empty!");
        include 'view/list_filter_data_pasien.php';
        break;
    case '27'            :
        if (!file_exists ("view/list_filter_kunjungan_pasien.php"))
            die ("list_filter_kunjungan_pasien.php File Empty!");
        include 'view/list_filter_kunjungan_pasien.php';
        break;
    case '28'            :
        if (!file_exists ("view/upd_kunjungan_pasien.php"))
            die ("upd_kunjungan_pasien.php File Empty!");
        include 'view/upd_kunjungan_pasien.php';
        break;
    case '29'            :
        if (!file_exists ("view/del_pendaftaran.php"))
            die ("del_pendaftaran.php File Empty!");
        include 'view/del_pendaftaran.php';
        break;

    case '31r'            :
        if (!file_exists ("view/menu_pembayaran_ranap.php"))
            die ("menu_pembayaran_ranap.php File Empty!");
        include 'view/menu_pembayaran_ranap.php';
        break;
    case '31s'            :
        if (!file_exists ("view/ranap/billranap.php"))
            die ("ranap/billranap.php File Empty!");
        include 'view/ranap/billranap.php';
        break;
    case '31'            :
        if (!file_exists ("view/rekap_pembayaran.php"))
            die ("rekap_pembayaran.php File Empty!");
        include 'view/rekap_pembayaran.php';
        break;
	case '31pertanggal'	:
        if (!file_exists ("view/rekap_pembayaran_pertanggal.php"))
            die ("rekap_pembayaran_pertanggal.php File Empty!");
        include 'view/rekap_pembayaran_pertanggal.php';
        break;
	case '31pertanggal_det'	:
        if (!file_exists ("view/rekap_pembayaran_pertanggal_det.php"))
            die ("rekap_pembayaran_pertanggal_det.php File Empty!");
        include 'view/rekap_pembayaran_pertanggal_det.php';
        break;
    case '32'            :
        if (!file_exists ("view/detil_pembayaran.php"))
            die ("detil_pembayaran.php File Empty!");
        include 'view/detil_pembayaran.php';
        break;
    case '32r'            :
        if (!file_exists ("view/detil_pembayaran_ranap.php"))
            die ("detil_pembayaran_ranap.php File Empty!");
        include 'view/detil_pembayaran_ranap.php';
        break;
    case '32rd'            :
        if (!file_exists ("view/detil_pembayaran_ranap_rinci.php"))
            die ("detil_pembayaran_ranap_rinci.php File Empty!");
        include 'view/detil_pembayaran_ranap_rinci.php';
        break;
    case '33'            :
        if (!file_exists ("view/billrajal.php"))
            die ("billrajal.php File Empty!");
        include 'view/billrajal.php';
        break;
	case '33_asuransi'            :
        if (!file_exists ("view/daftarklaim/billrajal.php"))
            die ("daftarklaim/billrajal.php File Empty!");
        include 'view/daftarklaim/billrajal.php';
        break;
	case '33a_asuransi'            :
        if (!file_exists ("view/daftarklaim/billranap.php"))
            die ("daftarklaim/billranap.php File Empty!");
        include 'view/daftarklaim/billranap.php';
        break;
    case '33batal'            :
        if (!file_exists ("view/billrajal_batal.php"))
            die ("billrajal_batal.php File Empty!");
        include 'view/billrajal_batal.php';
        break;
    case '33a'            :
        if (!file_exists ("view/billranap.php"))
            die ("billranap.php File Empty!");
        include 'view/billranap.php';
        break;
	case '33depo_rajal'            :
        if (!file_exists ("view/billdepo_rajal.php"))
            die ("billdepo_rajal.php File Empty!");
        include 'view/billdepo_rajal.php';
        break;
	case '33depo_ranap'            :
        if (!file_exists ("view/billdepo_ranap.php"))
            die ("billdepo_ranap.php File Empty!");
        include 'view/billdepo_ranap.php';
        break;
	case '33gizi_rajal'            :
        if (!file_exists ("view/billgizi_rajal.php"))
            die ("billgizi_rajal.php File Empty!");
        include 'view/billgizi_rajal.php';
        break;
	case '33gizi_ranap'            :
        if (!file_exists ("view/billgizi_ranap.php"))
            die ("billgizi_ranap.php File Empty!");
        include 'view/billgizi_ranap.php';
        break;
    case '33a_dup'            :
        if (!file_exists ("view/billranap_duplicate.php"))
            die ("billranap_duplicate.php File Empty!");
        include 'view/billranap_duplicate.php';
        break;		
    case '34'            :
        if (!file_exists ("view/cartbill.php"))
            die ("cartbill.php File Empty!");
        include 'view/cartbill.php';
        break;
	case '34gizi_rajal'            :
        if (!file_exists ("view/cartbill_gizirajal.php"))
            die ("cartbill_gizirajal.php File Empty!");
        include 'view/cartbill_gizirajal.php';
        break;
	case '34depo_rajal'            :
        if (!file_exists ("view/cartbill_deporajal.php"))
            die ("cartbill_deporajal.php File Empty!");
        include 'view/cartbill_deporajal.php';
        break;
	case '34depo_ranap'            :
        if (!file_exists ("view/cartbill_deporanap.php"))
            die ("cartbill_deporanap.php File Empty!");
        include 'view/cartbill_deporanap.php';
        break;
	case '34_asuransi'            :
        if (!file_exists ("view/daftarklaim/cartbill.php"))
            die ("daftarklaim/cartbill.php File Empty!");
        include 'view/daftarklaim/cartbill.php';
        break;
    case '34x'            :
        if (!file_exists ("view/rajal/cartbill.php"))
            die ("rajal/cartbill.php File Empty!");
        include 'view/rajal/cartbill.php';
        break;

    case '34a'            :
        if (!file_exists ("view/cartbillranap.php"))
            die ("cartbillranap.php File Empty!");
        include 'view/cartbillranap.php';
        break;
    case '34tb'            :
        if (!file_exists ("view/carttambahdeposit.php"))
            die ("carttambahdeposit.php File Empty!");
        include 'view/carttambahdeposit.php';
        break;
    case '35'            :
        if (!file_exists ("view/rekap_pembayaran_ranap.php"))
            die ("rekap_pembayaran_ranap.php File Empty!");
        include 'view/rekap_pembayaran_ranap.php';
        break;
	case '35pertanggal'            :
        if (!file_exists ("view/rekap_pembayaran_ranap_pertanggal.php"))
            die ("rekap_pembayaran_ranap_pertanggal.php File Empty!");
        include 'view/rekap_pembayaran_ranap_pertanggal.php';
        break;
	case '35pertanggal_det'            :
        if (!file_exists ("view/rekap_pembayaran_ranap_pertanggal_det.php"))
            die ("rekap_pembayaran_ranap_pertanggal_det.php File Empty!");
        include 'view/rekap_pembayaran_ranap_pertanggal_det.php';
        break;
    case '36'            :
        if (!file_exists ("view/detil_pembayaran_ranap.php"))
            die ("detil_pembayaran_ranap.php File Empty!");
        include 'view/detil_pembayaran_ranap.php';
        break;
    case '37'            :
        if (!file_exists ("view/cartbilldeposit.php"))
            die ("cartbilldeposit.php File Empty!");
        include 'view/cartbilldeposit.php';
        break;
    case 'p01'            :
        if (!file_exists ("view/pembayaran/rekap/menu_rekap.php"))
            die ("pembayaran/rekap/menu_rekap.php File Empty!");
        include 'view/pembayaran/rekap/menu_rekap.php';
        break;
    case 'p02'            :
        if (!file_exists ("view/pembayaran/rekap/filter_ssrd.php"))
            die ("pembayaran/rekap/filter_ssrd.php File Empty!");
        include 'view/pembayaran/rekap/filter_ssrd.php';
        break;
    case 'p03'            :
        if (!file_exists ("view/pembayaran/rekap/f_ssrd.php"))
            die ("pembayaran/rekap/f_ssrd.php File Empty!");
        include 'view/pembayaran/rekap/f_ssrd.php';
        break;
		
    case '5':
        if($_SESSION['KDUNIT']=="10") {
            if (!file_exists ("view/vk/listpasien_vk.php"))
                die ("vk/listpasien_vk.php File Empty!");
            include 'view/vk/listpasien_vk.php';	
		}elseif($_SESSION['KDUNIT']=="9") {
            if (!file_exists ("view/ugd/listpasien_ugd.php"))
                die ("ugd/listpasien_ugd.php File Empty!");
            include 'view/ugd/listpasien_ugd.php';	
        }else {
            if (!file_exists ("view/rajal/listpasien.php"))
                die ("rajal/listpasien.php File Empty!");
            include 'view/rajal/listpasien.php';
        }
        break;
	case '5ranap'            :
		if (!file_exists ("vk/listpasien_vkranap.php"))
			die ("vk/listpasien_vkranap.php File Empty!");
		include 'view/vk/listpasien_vkranap.php';	
		break;
	case '5tindakanranap'            :
		if (!file_exists ("vk/daftar_tindakan_vkranap.php"))
			die ("vk/daftar_tindakan_vkranap.php File Empty!");
		include 'view/vk/daftar_tindakan_vkranap.php';	
		break;
	case '5vkranap'            :
		if (!file_exists ("vk/vkranap.php"))
			die ("vk/vkranap.php File Empty!");
		include 'view/vk/vkranap.php';	
		break;
    case '51'            :
        if($_SESSION['KDUNIT']=="9") {
            if (!file_exists ("view/ugd/menuugd.php"))
                die ("ugd/menuugd.php File Empty!");
            include 'view/ugd/menuugd.php';
            break;
        }else if($_SESSION['KDUNIT']=="10") {
            if (!file_exists ("view/vk/menu_vk.php"))
                die ("vk/menu_vk.php File Empty!");
            include 'view/vk/menu_vk.php';
        }else if($_SESSION['KDUNIT']=="11") {
            if (!file_exists ("view/rujukan/surat_rujukan.php"))
                die ("rujukan/surat_rujukan.php File Empty!");
            include 'view/rujukan/surat_rujukan.php';
        }else {
            if (!file_exists ("view/rajal/menupoly.php"))
                die ("rajal/menupoly.php File Empty!");
            include 'view/rajal/menupoly.php';
        }
        break;

    case '52'            :
        if (!file_exists ("view/rajal/permintaanbarang.php"))
            die ("rajal/permintaanbarang.php File Empty!");
        include 'view/rajal/permintaanbarang.php';
        break;
    case '54'            :
        if($_SESSION['KDUNIT']=="10") {
            if (!file_exists ("view/vk/sensus_vk.php"))
                die ("vk/sensus_vk.php File Empty!");
            include 'view/vk/sensus_vk.php';
            break;
        }
        if (!file_exists ("view/rajal/sensusrajal.php"))
            die ("rajal/sensusrajal.php File Empty!");
        include 'view/rajal/sensusrajal.php';
        break;
    case '53'            :
        if (!file_exists ("view/rajal/listpermintaanbarang.php"))
            die ("rajal/listpermintaanbarang.php File Empty!");
        include 'view/rajal/listpermintaanbarang.php';
        break;
    case '55'            :
        if (!file_exists ("view/rajal/detail_hasil_lab.php"))
            die ("rajal/detail_hasil_lab.php File Empty!");
        include 'view/rajal/detail_hasil_lab.php';
        break;
    case '58'            :
        if (!file_exists ("view/rajal/plan_pengadaan_barang.php"))
            die ("rajal/plan_pengadaan_barang.php File Empty!");
        include 'view/rajal/plan_pengadaan_barang.php';
        break;
    case '59'            :
        if (!file_exists ("view/rajal/list_plan_pengadaan_barang.php"))
            die ("rajal/list_plan_pengadaan_barang.php File Empty!");
        include 'view/rajal/list_plan_pengadaan_barang.php';
        break;
    case '5x'            :
        if (!file_exists ("view/vk/rencana_keperawatan_perinatologi.htm"))
            die ("vk/rencana_keperawatan_perinatologi.htm File Empty!");
        include 'view/vk/rencana_keperawatan_perinatologi.htm';
        break;

    //lab
	case 'sisipan_lab'            :
        if (!file_exists ("view/lab/sisipan_lab.php"))
            die ("lab/sisipan_lab.php File Empty!");
        include 'view/lab/sisipan_lab.php';
        break;
	case 'detail_billing_lab'            :
        if (!file_exists ("view/lab/detail_billing_lab.php"))
            die ("lab/detail_billing_lab.php File Empty!");
        include 'view/lab/detail_billing_lab.php';
        break;
	case 'remove_billing'            :
        if (!file_exists ("view/lab/remove_billing.php"))
            die ("lab/remove_billing.php File Empty!");
        include 'view/lab/remove_billing.php';
        break;
		
	case 'formorderlab_tambahan'            :
        if (!file_exists ("view/lab/formorderlab_tambahan.php"))
            die ("lab/formorderlab_tambahan.php File Empty!");
        include 'view/lab/formorderlab_tambahan.php';
        break;
		
    case '6'            :
        if (!file_exists ("view/lab/list_order_lab.php"))
            die ("lab/list_order_lab.php File Empty!");
        include 'view/lab/list_order_lab.php';
        break;
	case '6order':
		if (!file_exists ("lab/list_periksalab.php"))
            die ("lab/list_periksalab.php File Empty!");
        include 'view/lab/list_periksalab.php';
        break;
	case '6order_aps':
		if (!file_exists ("lab/list_periksalab_aps.php"))
            die ("lab/list_periksalab_aps.php File Empty!");
        include 'view/lab/list_periksalab_aps.php';
        break;
    case '61'            :
        if (!file_exists ("view/lab/list_hasil_lab.php"))
            die ("lab/list_hasil_lab.php File Empty!");
        include 'view/lab/list_hasil_lab.php';
        break;
    case '62'            :
        if (!file_exists ("view/lab/orderlab.php"))
            die ("lab/orderlab.php File Empty!");
        include 'view/lab/orderlab.php';
        break;
	case '62formorderlab'            :
        if (!file_exists ("view/lab/formorderlab.php"))
            die ("lab/formorderlab.php File Empty!");
        include 'view/lab/formorderlab.php';
        break;
    case '63'            :
        if (!file_exists ("view/lab/detail_hasil_lab.php"))
            die ("lab/detail_hasil_lab.php File Empty!");
        include 'view/lab/detail_hasil_lab.php';
        break;
    case 'l01'            :
        if (!file_exists ("view/lab/pendaftaran_aps.php"))
            die ("lab/pendaftaran_aps.php File Empty!");
        include 'view/lab/pendaftaran_aps.php';
        break;
    case 'l02'            :
        if (!file_exists ("view/lab/add_lab_aps.php"))
            die ("lab/add_lab_aps.php File Empty!");
        include 'view/lab/add_lab_aps.php';
        break;
    case 'l03'            :
        if (!file_exists ("view/lab/list_order_aps.php"))
            die ("lab/list_order_aps.php File Empty!");
        include 'view/lab/list_order_aps.php';
        break;
    case 'l04'            :
        if (!file_exists ("view/lab/orderlab_aps.php"))
            die ("lab/orderlab_aps.php File Empty!");
        include 'view/lab/orderlab_aps.php';
        break;
    case 'l05'            :
        if (!file_exists ("view/lab/reg_pelayanan.php"))
            die ("lab/reg_pelayanan.php File Empty!");
        include 'view/lab/reg_pelayanan.php';
        break;
    case 'lab1'            :
        if (!file_exists ("view/lab/change_orderlab.php"))
            die ("lab/change_orderlab.php File Empty!");
        include 'view/lab/change_orderlab.php';
        break;
    case '7'            :
        if (!file_exists ("view/radiologi/listradiologi.php"))
            die ("radiologi/listradiologi.php File Empty!");
        include 'view/radiologi/listradiologi.php';
        break;
	case '7order':
		if (!file_exists ("radiologi/list_periksarad.php"))
            die ("radiologi/list_periksarad.php File Empty!");
        include 'view/radiologi/list_periksarad.php';
        break;
    case '71'            :
        if (!file_exists ("view/radiologi/list_hasilradiologi.php"))
            die ("radiologi/list_hasilradiologi.php File Empty!");
        include 'view/radiologi/list_hasilradiologi.php';
        break;
	case 'list_pasien_ranap_rad':
		if (!file_exists ("radiologi/list_pasien_ranap_rad.php"))
            die ("radiologi/list_pasien_ranap_rad.php File Empty!");
        include 'view/radiologi/list_pasien_ranap_rad.php';
        break;
	case 'list_pasien_rajal_rad':
		if (!file_exists ("radiologi/list_pasien_rajal_rad.php"))
            die ("radiologi/list_pasien_rajal_rad.php File Empty!");
        include 'view/radiologi/list_pasien_rajal_rad.php';
        break;
	case '72formorderrad_ranap':
		if (!file_exists ("radiologi/formorderrad_ranap.php"))
            die ("radiologi/formorderrad_ranap.php File Empty!");
        include 'view/radiologi/formorderrad_ranap.php';
        break;
	case '72formorderrad_rajal':
		if (!file_exists ("radiologi/formorderrad_rajal.php"))
            die ("radiologi/formorderrad_rajal.php File Empty!");
        include 'view/radiologi/formorderrad_rajal.php';
        break;
	case '72formorderrad'            :
        if (!file_exists ("view/radiologi/formorderrad.php"))
            die ("radiologi/formorderrad.php File Empty!");
        include 'view/radiologi/formorderrad.php';
        break;
    case '72'            :
        if (!file_exists ("view/radiologi/hasillihat.php"))
            die ("radiologi/hasillihat.php File Empty!");
        include 'view/radiologi/hasillihat.php';
        break;
    case '73'            :
        if (!file_exists ("view/radiologi/periksa.php"))
            die ("radiologi/periksa.php File Empty!");
        include 'view/radiologi/periksa.php';
        break;
    case '74'            :
        if (!file_exists ("view/radiologi/rekap_radiologi.php"))
            die ("radiologi/rekap_radiologi.php File Empty!");
        include 'view/radiologi/rekap_radiologi.php';
        break;
    case '75'            :
        if (!file_exists ("view/radiologi/hasil_dokter.php"))
            die ("radiologi/hasil_dokter.php File Empty!");
        include 'view/radiologi/hasil_dokter.php';
        break;
    case 'rd1'            :
        if (!file_exists ("view/radiologi/orderbarang/pengeluaranbarang.php"))
            die ("radiologi/orderbarang/pengeluaranbarang.php File Empty!");
        include("radiologi/orderbarang/pengeluaranbarang.php");
        break;

    case 'r01'            :
        if (!file_exists ("view/radiologi/pendaftaran_aps.php"))
            die ("radiologi/pendaftaran_aps.php File Empty!");
        include 'view/radiologi/pendaftaran_aps.php';
        break;
    case 'r02'            :
        if (!file_exists ("view/radiologi/add_rad_aps.php"))
            die ("radiologi/add_rad_aps.php File Empty!");
        include 'view/radiologi/add_rad_aps.php';
        break;
    case 'r03'            :
        if (!file_exists ("view/radiologi/list_order_aps.php"))
            die ("radiologi/list_order_aps.php File Empty!");
        include 'view/radiologi/list_order_aps.php';
        break;
    case 'r04'            :
        if (!file_exists ("view/radiologi/periksa_aps.php"))
            die ("radiologi/periksa_aps.php File Empty!");
        include 'view/radiologi/periksa_aps.php';
        break;
    case 'r05'            :
        if (!file_exists ("view/radiologi/list_hasilradiologi_aps.php"))
            die ("radiologi/list_hasilradiologi_aps.php File Empty!");
        include 'view/radiologi/list_hasilradiologi_aps.php';
        break;
    case 'r06'            :
        if (!file_exists ("view/radiologi/hasil_dokter_aps.php"))
            die ("radiologi/hasil_dokter_aps.php File Empty!");
        include 'view/radiologi/hasil_dokter_aps.php';
        break;
	case 'billaps':
		if (!file_exists ("bill_aps.php"))
            die ("bill_aps.php File Empty!");
        include 'view/bill_aps.php';
        break;
	case 'cartbill_aps':
		if (!file_exists ("cartbill_aps.php"))
            die ("cartbill_aps.php File Empty!");
        include 'view/cartbill_aps.php';
        break;

//menu gudang

    case '8'            :
        if (!file_exists ("view/gudang/listpermintaanbarang.php"))
            die ("gudang/listpermintaanbarang.php File Empty!");
        include 'view/gudang/listpermintaanbarang.php';
        break;
    case '81'            :
        if (!file_exists ("view/gudang/permintaanbarang.php"))
            die ("gudang/permintaanbarang.php File Empty!");
        include 'view/gudang/permintaanbarang.php';
        break;
    case '82'            :
        if (!file_exists ("view/gudang/listmasterbarang.php"))
            die ("gudang/listmasterbarang.php File Empty!");
        include 'view/gudang/listmasterbarang.php';
        break;
    case '83'            :
        if (!file_exists ("view/gudang/penerimaanbarang.php"))
            die ("gudang/penerimaanbarang.php File Empty!");
        include 'view/gudang/penerimaanbarang.php';
        break;
    case 'x83'            :
        if (!file_exists ("view/gudang/listhistoripenerimaan.php"))
            die ("gudang/listhistoripenerimaan.php File Empty!");
        include 'view/gudang/listhistoripenerimaan.php';
        break;
    case '84'            :
        if (!file_exists ("view/gudang/menu_laporan.php"))
            die ("gudang/menu_laporan.php File Empty!");
        include 'view/gudang/menu_laporan.php';
        break;
    case '85'            :
        if (!file_exists ("view/gudang/listhistoripermintaan.php"))
            die ("gudang/listhistoripermintaan.php File Empty!");
        include 'view/gudang/listhistoripermintaan.php';
        break;
    case '86'            :
        if (!file_exists ("view/gudang/historipermintaanbarang.php"))
            die ("gudang/historipermintaanbarang.php File Empty!");
        include 'view/gudang/historipermintaanbarang.php';
        break;
    case '89'            :
        if (!file_exists ("view/gudang/rencanapengadaan.php"))
            die ("gudang/rencanapengadaan.php File Empty!");
        include 'view/gudang/rencanapengadaan.php';
        break;
    case '9'            :
        if (!file_exists ("view/gudang/listpermintaanbarang.php"))
            die ("gudang/listpermintaanbarang.php File Empty!");
        include 'view/gudang/listpermintaanbarang.php';
        break;

    case 'x83'            :
        if (!file_exists ("view/gudang/listpengembalianbarang.php"))
            die ("gudang/listpengembalianbarang.php File Empty!");
        include 'view/gudang/listpengembalianbarang.php';
        break;

    case 'y83'            :
        if (!file_exists ("view/gudang/pengembalianbarang.php"))
            die ("gudang/pengembalianbarang.php File Empty!");
        include 'view/gudang/pengembalianbarang.php';
        break;

    case 'g01'            :
        if (!file_exists ("view/gudang/laporan_harian.php"))
            die ("gudang/laporan_harian.php File Empty!");
        include 'view/gudang/laporan_harian.php';
        break;

    case 'g02'            :
        if (!file_exists ("view/gudang/laporan_bulanan.php"))
            die ("gudang/laporan_bulanan.php File Empty!");
        include 'view/gudang/laporan_bulanan.php';
        break;

    case 'g03'            :
        if (!file_exists ("view/gudang/rekap_bulanan.php"))
            die ("gudang/rekap_bulanan.php File Empty!");
        include 'view/gudang/rekap_bulanan.php';
        break;

    case 'g04'            :
        if (!file_exists ("view/gudang/rekap_triwulan.php"))
            die ("gudang/rekap_triwulan.php File Empty!");
        include 'view/gudang/rekap_triwulan.php';
        break;

    case 'g05'            :
        if (!file_exists ("view/gudang/rekap_tahunan.php"))
            die ("gudang/rekap_tahunan.php File Empty!");
        include 'view/gudang/rekap_tahunan.php';
        break;

    case 'g06'            :
        if (!file_exists ("view/gudang/laporan_bulanan_unit.php"))
            die ("gudang/laporan_bulanan_unit.php File Empty!");
        include 'view/gudang/laporan_bulanan_unit.php';
        break;
    case 'g06x'            :
        if (!file_exists ("view/gudang/laporan_bulanan_unit_detail.php"))
            die ("gudang/laporan_bulanan_unit_detail.php File Empty!");
        include 'view/gudang/laporan_bulanan_unit_detail.php';
        break;


// end menu gudang		

//menu apotek
	
	case 'form_add_obatracik'            :
        if (!file_exists ("view/apotek/form_add_obatracik.php"))
            die ("apotek/form_add_obatracik.php File Empty!");
        include 'view/apotek/form_add_obatracik.php';
        break;
	case 'pengembalian_resep'            :
        if (!file_exists ("view/apotek/pengembalian_resep.php"))
            die ("apotek/pengembalian_resep.php File Empty!");
        include 'view/apotek/pengembalian_resep.php';
        break;
	case 'detail_resep'            :
        if (!file_exists ("view/apotek/detail_resep.php"))
            die ("apotek/detail_resep.php File Empty!");
        include 'view/apotek/detail_resep.php';
        break;
	case 'detail_resep_aps'            :
        if (!file_exists ("view/apotek/detail_resep_aps.php"))
            die ("apotek/detail_resep_aps.php File Empty!");
        include 'view/apotek/detail_resep_aps.php';
        break;
	case 'list_obat_rajal'            :
        if (!file_exists ("view/apotek/list_obat_rajal.php"))
            die ("apotek/list_obat_rajal.php File Empty!");
        include 'view/apotek/list_obat_rajal.php';
        break;
	case 'list_obat_aps'            :
        if (!file_exists ("view/apotek/list_obat_aps.php"))
            die ("apotek/list_obat_aps.php File Empty!");
        include 'view/apotek/list_obat_aps.php';
        break;
	case 'list_obat_ranap'            :
        if (!file_exists ("view/apotek/list_obat_ranap.php"))
            die ("apotek/list_obat_ranap.php File Empty!");
        include 'view/apotek/list_obat_ranap.php';
        break;
	case 'list_pasien_apotek_rajal'            :
        if (!file_exists ("view/apotek/list_pasien_apotek_rajal.php"))
            die ("apotek/list_pasien_apotek_rajal.php File Empty!");
        include 'view/apotek/list_pasien_apotek_rajal.php';
        break;
	case 'list_pasien_apotek_aps'            :
        if (!file_exists ("view/apotek/list_pasien_apotek_aps.php"))
            die ("apotek/list_pasien_apotek_aps.php File Empty!");
        include 'view/apotek/list_pasien_apotek_aps.php';
        break;
	case 'list_pasien_apotek_ranap'            :
        if (!file_exists ("view/apotek/list_pasien_apotek_ranap.php"))
            die ("apotek/list_pasien_apotek_ranap.php File Empty!");
        include 'view/apotek/list_pasien_apotek_ranap.php';
        break;
	case 'add_resep'            :
        if (!file_exists ("view/apotek/add_resep.php"))
            die ("apotek/add_resep.php File Empty!");
        include 'view/apotek/add_resep.php';
        break;
	case 'add_resep_aps'            :
        if (!file_exists ("view/apotek/add_resep_aps.php"))
            die ("apotek/add_resep_aps.php File Empty!");
        include 'view/apotek/add_resep_aps.php';
        break;
		
    case '10'            :
        if (!file_exists ("view/apotek/listpermintaanresep.php"))
            die ("apotek/listpermintaanresep.php File Empty!");
        include 'view/apotek/listpermintaanresep.php';
        break;
    case '101'            :
        if (!file_exists ("view/apotek/addresep.php"))
            die ("apotek/addresep.php File Empty!");
        include 'view/apotek/addresep.php';
        break;
    case '107'            :
        if (!file_exists ("view/apotek/listhistoripermintaan.php"))
            die ("apotek/listhistoripermintaan.php File Empty!");
        include 'view/apotek/listhistoripermintaan.php';
        break;
    case '108'            :
        if (!file_exists ("view/apotek/historipermintaanresep.php"))
            die ("apotek/historipermintaanresep.php File Empty!");
        include 'view/apotek/historipermintaanresep.php';
        break;
    case '113'            :
        if (!file_exists ("view/apotek/printresep.php"))
            die ("apotek/printresep.php File Empty!");
        include 'view/apotek/printresep.php';
        break;
    case '114'            :
        if (!file_exists ("view/apotek/rekap_resep.php"))
            die ("apotek/rekap_resep.php File Empty!");
        include 'view/apotek/rekap_resep.php';
        break;
    case '115'            :
        if (!file_exists ("view/apotek/sensus_layanan.php"))
            die ("apotek/sensus_layanan.php File Empty!");
        include 'view/apotek/sensus_layanan.php';
        break;
    case '110'            :
        if (!file_exists ("view/apotek/sensus_layanan.php"))
            die ("apotek/sensus_layanan.php File Empty!");
        include 'view/apotek/sensus_layanan.php';
        break;
    case '110x'            :
        if (!file_exists ("view/apotek/pemantauan_resep.php"))
            die ("apotek/pemantauan_resep.php File Empty!");
        include 'view/apotek/pemantauan_resep.php';
        break;
	case '110xt'            :
        if (!file_exists ("view/apotek/pemantauan_resep_ranap.php"))
            die ("apotek/pemantauan_resep_ranap.php File Empty!");
        include 'view/apotek/pemantauan_resep_ranap.php';
        break;
//end menu apotek

    //menu farmasi
    case 'f01'            :
        if (!file_exists ("view/orderbarang/permintaanbarang.php"))
            die ("orderbarang/permintaanbarang.php File Empty!");
        include 'view/orderbarang/permintaanbarang.php';
        break;
    case 'f02'            :
        if (!file_exists ("view/orderbarang/listpermintaanbarang.php"))
            die ("orderbarang/listpermintaanbarang.php File Empty!");
        include 'view/orderbarang/listpermintaanbarang.php';
        break;
    case 'f03'            :
        if (!file_exists ("view/orderbarang/penerimaanbarang.php"))
            die ("orderbarang/penerimaanbarang.php File Empty!");
        include 'view/orderbarang/penerimaanbarang.php';
        break;
    case 'f04'            :
        if (!file_exists ("view/orderbarang/pengeluaranbarang.php"))
            die ("orderbarang/pengeluaranbarang.php File Empty!");
        include 'view/orderbarang/pengeluaranbarang.php';
        break;
    case 'f05'            :
        if (!file_exists ("view/orderbarang/listpenerimaanbarang.php"))
            die ("orderbarang/listpenerimaanbarang.php File Empty!");
        include 'view/orderbarang/listpenerimaanbarang.php';
        break;
    case 'f06'            :
        if (!file_exists ("view/orderbarang/listpengeluaranbarang.php"))
            die ("orderbarang/listpengeluaranbarang.php File Empty!");
        include 'view/orderbarang/listpengeluaranbarang.php';
        break;
    case 'f07'            :
        if (!file_exists ("view/orderbarang/pengadaanbarang.php"))
            die ("orderbarang/pengadaanbarang.php File Empty!");
        include 'view/orderbarang/pengadaanbarang.php';
        break;
    case 'f08'            :
        if (!file_exists ("view/orderbarang/listpengadaanbarang.php"))
            die ("orderbarang/listpengadaanbarang.php File Empty!");
        include 'view/orderbarang/listpengadaanbarang.php';
        break;
    case 'f09'            :
        if (!file_exists ("view/orderbarang/filterlaporan_bulanan.php"))
            die ("orderbarang/filterlaporan_bulanan.php File Empty!");
        include 'view/orderbarang/filterlaporan_bulanan.php';
        break;
    case 'f10'            :
        if (!file_exists ("view/orderbarang/laporan_bulanan.php"))
            die ("orderbarang/laporan_bulanan.php File Empty!");
        include 'view/orderbarang/laporan_bulanan.php';
        break;
    case 'f09'            :
        if (!file_exists ("view/orderbarang/filterlaporan_bulanan.php"))
            die ("orderbarang/filterlaporan_bulanan.php File Empty!");
        include 'view/orderbarang/filterlaporan_bulanan.php';
        break;
    case 'f11'            :
        if (!file_exists ("view/orderbarang/filterlaporan_harian.php"))
            die ("orderbarang/filterlaporan_harian.php File Empty!");
        include 'view/orderbarang/filterlaporan_harian.php';
        break;
    case 'f12'            :
        if (!file_exists ("view/orderbarang/laporan_harian.php"))
            die ("orderbarang/laporan_harian.php File Empty!");
        include 'view/orderbarang/laporan_harian.php';
        break;
    case 'f12'            :
        if (!file_exists ("view/orderbarang/laporan_harian.php"))
            die ("orderbarang/laporan_harian.php File Empty!");
        include 'view/orderbarang/laporan_harian.php';
        break;
    case 'f44'            :
        if (!file_exists ("view/orderbarang/listpengeluaranbarang_pasien.php"))
            die ("orderbarang/listpengeluaranbarang_pasien.php File Empty!");
        include 'view/orderbarang/listpengeluaranbarang_pasien.php';
        break;
    case 'f21'            :
        if (!file_exists ("view/orderbarang/pengembalianbarang.php"))
            die ("orderbarang/pengembalianbarang.php File Empty!");
        include 'view/orderbarang/pengembalianbarang.php';
        break;
    case 'f22'            :
        if (!file_exists ("view/orderbarang/listpengembalianbarang.php"))
            die ("orderbarang/listpengembalianbarang.php File Empty!");
        include 'view/orderbarang/listpengembalianbarang.php';
        break;
    case 'f66'            :
        if (!file_exists ("view/orderbarang/filterbulanan_unit.php"))
            die ("orderbarang/filterbulanan_unit.php File Empty!");
        include 'view/orderbarang/filterbulanan_unit.php';
        break;
    case 'f66x'            :
        if (!file_exists ("view/orderbarang/laporan_bulanan_stok_unit.php"))
            die ("orderbarang/laporan_bulanan_stok_unit.php File Empty!");
        include 'view/orderbarang/laporan_bulanan_stok_unit.php';
        break;
    case 'f66y'            :
        if (!file_exists ("view/orderbarang/laporan_bulanan_unit_detail.php"))
            die ("orderbarang/laporan_bulanan_unit_detail.php File Empty!");
        include 'view/orderbarang/laporan_bulanan_unit_detail.php';
        break;
    //end menu farmasi

    case '11'            :
        if (!file_exists ("view/ugd/list_kunjungan_pasien.php"))
            die ("ugd/list_kunjungan_pasien.php File Empty!");
        include 'view/ugd/list_kunjungan_pasien.php';
        break;
    case '111'            :
        if (!file_exists ("view/ugd/pemeriksaan.php"))
            die ("ugd/pemeriksaan.php File Empty!");
        include 'view/ugd/pemeriksaan.php';
        break;
    case '12'            :
        if (!file_exists ("view/ranap/list_ranap.php"))
            die ("ranap/list_ranap.php File Empty!");
        include 'view/ranap/list_ranap.php';
        break;
    case '121'            :
        if (!file_exists ("view/ranap/ranap.php"))
            die ("ranap/ranap.php File Empty!");
        include 'view/ranap/ranap.php';
        break;
    case '129x'            :
        if (!file_exists ("view/ranap/list_perm_makan.php"))
            die ("ranap/list_perm_makan.php File Empty!");
        include 'view/ranap/list_perm_makan.php';
        break;
    case '122'            :
        if (!file_exists ("view/rm/sensusranap.php"))
            die ("rm/sensusranap.php File Empty!");
        include 'view/rm/sensusranap.php';
        break;
	case '122harian'            :
        if (!file_exists ("view/rm/sensusranap_harian.php"))
            die ("rm/sensusranap_harian.php File Empty!");
        include 'view/rm/sensusranap_harian.php';
        break;
	case '122harianpasienkeluar'            :
        if (!file_exists ("view/rm/sensusranap_harianpasienkeluar.php"))
            die ("rm/sensusranap_harianpasienkeluar.php File Empty!");
        include 'view/rm/sensusranap_harianpasienkeluar.php';
        break;
    case '122x'            :
        if (!file_exists ("view/report_rm/bukuregister_ranap.php"))
            die ("report_rm/bukuregister_ranap.php File Empty!");
        include 'view/report_rm/bukuregister_ranap.php';
        break;
    case '123'            :
        if (!file_exists ("view/orderbarang/pengeluaranbarang.php"))
            die ("orderbarang/pengeluaranbarang.php File Empty!");
        include 'view/orderbarang/pengeluaranbarang.php';
        break;
    case '124'            :
        if (!file_exists ("view/admission/list_room_pengganti.php"))
            die ("admission/list_room_pengganti.php File Empty!");
        include 'view/admission/list_room_pengganti.php';
        break;
    case '125'            :
        if (!file_exists ("view/admission/form_pencarian.php"))
            die ("admission/form_pencarian.php File Empty!");
        include 'view/admission/form_pencarian.php';
        break;

    //rm
    case '13'            :
        if (!file_exists ("view/rm/list_tracer.php"))
            die ("rm/list_tracer.php File Empty!");
        include 'view/rm/list_tracer.php';
        break;
    case '13m'            :
        if (!file_exists ("view/rm/menu_rm.php"))
            die ("rm/menu_rm.php File Empty!");
        include 'view/rm/menu_rm.php';
        break;
    case '13n'            :
        if (!file_exists ("view/rm/menu_rekap.php"))
            die ("rm/menu_rekap.php File Empty!");
        include 'view/rm/menu_rekap.php';
        break;

    case '131'            :
        if (!file_exists ("view/rm/list_filter_tracer.php"))
            die ("rm/list_filter_tracer.php File Empty!");
        include 'view/rm/list_filter_tracer.php';
        break;
    case '132'            :
        if (!file_exists ("view/rm/menurm.php"))
            die ("rm/menurm.php File Empty!");
        include 'view/rm/menurm.php';
        break;
    case '133'            :
        if (!file_exists ("view/rm/sensusrajal.php"))
            die ("rm/sensusrajal.php File Empty!");
        include 'view/rm/sensusrajal.php';
        break;
    case '134'            :
        if (!file_exists ("view/rm/sensusranap.php"))
            die ("rm/sensusranap.php File Empty!");
        include 'view/rm/sensusranap.php';
        break;
    case '135'            :
        if (!file_exists ("view/rm/listranap.php"))
            die ("listranap.php File Empty!");
        include 'view/rm/listranap.php';
        break;
    case '136'            :
        if (!file_exists ("view/report_rm/lapharian_ok.php"))
            die ("report_rm/lapharian_ok.php File Empty!");
        include 'view/report_rm/lapharian_ok.php';
        break;
    case '137'            :
        if (!file_exists ("view/report_rm/lapharian_perinatologi.php"))
            die ("report_rm/lapharian_perinatologi.php File Empty!");
        include 'view/report_rm/lapharian_perinatologi.php';
        break;
    case '138'            :
        if (!file_exists ("view/report_rm/lapharian_ranap.php"))
            die ("report_rm/lapharian_ranap.php File Empty!");
        include 'view/report_rm/lapharian_ranap.php';
        break;
    case '139'            :
        if (!file_exists ("view/report_rm/lapharian_vk.php"))
            die ("report_rm/lapharian_vk.php File Empty!");
        include 'view/report_rm/lapharian_vk.php';
        break;
    case '140'            :
        if (!file_exists ("view/report_rm/sensus_pendaftaran_rajal.php"))
            die ("report_rm/sensus_pendaftaran_rajal.php File Empty!");
        include 'view/report_rm/sensus_pendaftaran_rajal.php';
        break;
    case '1310'            :
        if (!file_exists ("view/report_rm/sensus_farmasi.php"))
            die ("report_rm/sensus_farmasi.php File Empty!");
        include 'view/report_rm/sensus_farmasi.php';
        break;
    case '1311'            :
        if (!file_exists ("view/report_rm/sensus_lab.php"))
            die ("report_rm/sensus_lab.php File Empty!");
        include 'view/report_rm/sensus_lab.php';
        break;
    case '1312'            :
        if (!file_exists ("view/report_rm/sensus_layanan.php"))
            die ("report_rm/sensus_layanan.php File Empty!");
        include 'view/report_rm/sensus_layanan.php';
        break;
    case '1313'            :
        if (!file_exists ("view/report_rm/sensus_radiologi.php"))
            die ("report_rm/sensus_radiologi.php File Empty!");
        include 'view/report_rm/sensus_radiologi.php';
        break;
    case '1314'            :
        if (!file_exists ("view/report_rm/sensusharian_rajal.php"))
            die ("report_rm/sensusharian_rajal.php File Empty!");
        include 'view/report_rm/sensusharian_rajal.php';
        break;
    case '1315'            :
        if (!file_exists ("view/report_rm/sensusharian_ranap.php"))
            die ("report_rm/sensusharian_ranap.php File Empty!");
        include 'view/report_rm/sensusharian_ranap.php';
        break;
    case '1316'            :
        if (!file_exists ("view/report_rm/sensusharian_ugd.php"))
            die ("report_rm/sensusharian_ugd.php File Empty!");
        include 'view/report_rm/sensusharian_ugd.php';
        break;
    case '13xtrnal'            :
        if (!file_exists ("view/rm/ex_report.php"))
            die ("rm/ex_report.php File Empty!");
        include 'view/rm/ex_report.php';
        break;

    case 'rm4'            :
        if (!file_exists ("view/rm/list_histori_pasien.php"))
            die ("rm/list_histori_pasien.php File Empty!");
        include 'view/rm/list_histori_pasien.php';
        break;
    case 'rm5'            :
        if (!file_exists ("view/rm/list_histori_pasien_ranap.php"))
            die ("rm/list_histori_pasien_ranap.php File Empty!");
        include 'view/rm/list_histori_pasien_ranap.php';
        break;
    case 'rm6'            :
        if (!file_exists ("view/rm/histori_pasien_detail.php"))
            die ("rm/histori_pasien_detail.php File Empty!");
        include 'view/rm/histori_pasien_detail.php';
        break;
	case 'rm6r'            :
        if (!file_exists ("view/rm/radiologi_info.php"))
            die ("radiologi_info.php File Empty!");
        include 'view/rm/radiologi_info.php';
        break;
	case 'rm6l'            :
        if (!file_exists ("view/rm/lab_info.php"))
            die ("lab_info.php File Empty!");
        include 'view/rm/lab_info.php';
        break;
    case 'rm7'            :
        if (!file_exists ("view/rm/histori_pasien_detail_ranap.php"))
            die ("rm/histori_pasien_detail_ranap.php File Empty!");
        include 'view/rm/histori_pasien_detail_ranap.php';
        break;

    case '140R'            :
        if (!file_exists ("view/report_rm/RekapanPendaftaranRawatJalan.php"))
            die ("report_rm/RekapanPendaftaranRawatJalan.php File Empty!");
        include 'view/report_rm/RekapanPendaftaranRawatJalan.php';
        break;
    case '141R'            :
        if (!file_exists ("view/report_rm/RekapanPolyRawatJalan.php"))
            die ("report_rm/RekapanPolyRawatJalan.php File Empty!");
        include 'view/report_rm/RekapanPolyRawatJalan.php';
        break;
    case '142R'            :
        if (!file_exists ("view/report_rm/RekapSensusPendaftaranRanap.php"))
            die ("report_rm/RekapSensusPendaftaranRanap.php File Empty!");
        include 'view/report_rm/RekapSensusPendaftaranRanap.php';
        break;
    case '143R'            :
        if (!file_exists ("view/report_rm/RekapMordibitasRajal.php"))
            die ("report_rm/RekapMordibitasRajal.php File Empty!");
        include 'view/report_rm/RekapMordibitasRajal.php';
        break;
	case '144R'            :
        if (!file_exists ("view/report_rm/sensus_pulang_rajal.php"))
            die ("report_rm/sensus_pulang_rajal.php File Empty!");
        include 'view/report_rm/sensus_pulang_rajal.php';
        break;
	case '144Rdet'            :
        if (!file_exists ("view/report_rm/sensus_pulangdet_rajal.php"))
            die ("report_rm/sensus_pulangdet_rajal.php File Empty!");
        include 'view/report_rm/sensus_pulangdet_rajal.php';
        break;
    case 'RL2A'            :
        if (!file_exists ("view/rm/rl2a.php"))
            die ("rm/rl2a.php File Empty!");
        include 'view/rm/rl2a.php';
        break;
    case 'RL2B'            :
        if (!file_exists ("view/rm/rl2b.php"))
            die ("rm/rl2b.php File Empty!");
        include 'view/rm/rl2b.php';
        break;
    case 'RL2A1'            :
        if (!file_exists ("view/rm/rl2a1.php"))
            die ("rm/rl2a1.php File Empty!");
        include 'view/rm/rl2a1.php';
        break;
    case 'RL2B1'            :
        if (!file_exists ("view/rm/rl2b1.php"))
            die ("rm/rl2b1.php File Empty!");
        include 'view/rm/rl2b1.php';
        break;

    case 'iso2'            :
        if (!file_exists ("view/rm/iso_pendaftaran.php"))
            die ("rm/iso_pendaftaran.php File Empty!");
        include 'view/rm/iso_pendaftaran.php';
        break;

//jamkesmas--------------------------------------
	case '14_askes'            :
        if (!file_exists ("view/daftarklaim/list_pendaftaran_askes.php"))
            die ("daftarklaim/list_pendaftaran_askes.php File Empty!");
        include 'view/daftarklaim/list_pendaftaran_askes.php';
        break;
    case '14'            :
        if (!file_exists ("view/daftarklaim/upload_form.php"))
            die ("daftarklaim/upload_form.php File Empty!");
        include 'view/daftarklaim/upload_form.php';
        break;
    case '14h'            :
        if (!file_exists ("view/daftarklaim/history.php"))
            die ("daftarklaim/history.php File Empty!");
        include 'view/daftarklaim/history.php';
        break;
    case '14_'            :
        if (!file_exists ("view/daftarklaim/list_pendaftaran.php"))
            die ("daftarklaim/list_pendaftaran.php File Empty!");
        include 'view/daftarklaim/list_pendaftaran.php';
        break;
    case '14verifikasi'            :
        if (!file_exists ("view/daftarklaim/verifikasi_data.php"))
            die ("daftarklaim/verifikasi_data.php File Empty!");
        include 'view/daftarklaim/verifikasi_data.php';
        break;
    case '14rujukan'            :
        if (!file_exists ("view/daftarklaim/rujukan.php"))
            die ("daftarklaim/rujukan.php File Empty!");
        include 'view/daftarklaim/rujukan.php';
        break;
    case '14vrujukan'            :
        if (!file_exists ("view/daftarklaim/valid_rujukan.php"))
            die ("daftarklaim/valid_rujukan.php File Empty!");
        include 'view/daftarklaim/valid_rujukan.php';
        break;
    case '14fjp'            :
        if (!file_exists ("view/daftarklaim/fjp.php"))
            die ("daftarklaim/fjp.php File Empty!");
        include 'view/daftarklaim/fjp.php';
        break;
    case '14vfjp'            :
        if (!file_exists ("view/daftarklaim/valid_fjp.php"))
            die ("daftarklaim/valid_fjp.php File Empty!");
        include 'view/daftarklaim/valid_fjp.php';
        break;
    case '141'            :
        if (!file_exists ("view/daftarklaim/rekap_data_ditolak.php"))
            die ("daftarklaim/rekap_data_ditolak.php File Empty!");
        include 'view/daftarklaim/rekap_data_ditolak.php';
        break;
    case '142'            :
        if (!file_exists ("view/daftarklaim/form_klaim_rawat_jalan.php"))
            die ("daftarklaim/form_klaim_rawat_jalan.php File Empty!");
        include 'view/daftarklaim/form_klaim_rawat_jalan.php';
        break;
    case '143'            :
        if (!file_exists ("view/daftarklaim/form_klaim_rawat_inap.php"))
            die ("daftarklaim/form_klaim_rawat_inap.php File Empty!");
        include 'view/daftarklaim/form_klaim_rawat_inap.php';
        break;
	case '144_rekap'            :
        if (!file_exists ("view/daftarklaim/rekap_asuransi.php"))
            die ("daftarklaim/rekap_asuransi.php File Empty!");
        include 'view/daftarklaim/rekap_asuransi.php';
        break;
	case '144_pertanggal'            :
        if (!file_exists ("view/daftarklaim/rekap_asuransi_pertanggal.php"))
            die ("daftarklaim/rekap_asuransi_pertanggal.php File Empty!");
        include 'view/daftarklaim/rekap_asuransi_pertanggal.php';
        break;
	case '144_pertanggal_det'            :
        if (!file_exists ("view/daftarklaim/rekap_asuransi_pertanggal_det.php"))
            die ("daftarklaim/rekap_asuransi_pertanggal_det.php File Empty!");
        include 'view/daftarklaim/rekap_asuransi_pertanggal_det.php';
        break;
	case '144_rekap_ranap'            :
        if (!file_exists ("view/daftarklaim/rekap_asuransi_ranap.php"))
            die ("daftarklaim/rekap_asuransi_ranap.php File Empty!");
        include 'view/daftarklaim/rekap_asuransi_ranap.php';
        break;
	case '144_ranap_pertanggal'            :
        if (!file_exists ("view/daftarklaim/rekap_asuransi_ranap_pertanggal.php"))
            die ("daftarklaim/rekap_asuransi_ranap_pertanggal.php File Empty!");
        include 'view/daftarklaim/rekap_asuransi_ranap_pertanggal.php';
        break;
	case '144_ranap_pertanggal_det'            :
        if (!file_exists ("view/daftarklaim/rekap_asuransi_ranap_pertanggal_det.php"))
            die ("daftarklaim/rekap_asuransi_ranap_pertanggal_det.php File Empty!");
        include 'view/daftarklaim/rekap_asuransi_ranap_pertanggal_det.php';
        break;
    case '144'            :
        if (!file_exists ("view/daftarklaim/form_rekapitulasi.php"))
            die ("daftarklaim/form_rekapitulasi.php File Empty!");
        include 'view/daftarklaim/form_rekapitulasi.php';
        break;
    case '145'            :
        if (!file_exists ("view/daftarklaim/verifikasi_rawat_jalan.php"))
            die ("daftarklaim/verifikasi_rawat_jalan.php File Empty!");
        include 'view/daftarklaim/verifikasi_rawat_jalan.php';
        break;
    case '146'            :
        if (!file_exists ("view/daftarklaim/verifikasi_rawat_inap.php"))
            die ("daftarklaim/verifikasi_rawat_inap.php File Empty!");
        include 'view/daftarklaim/verifikasi_rawat_inap.php';
        break;

    case '147'            :
        if (!file_exists ("view/daftarklaim/detail_rekap_data_ditolak.php"))
            die ("daftarklaim/detail_rekap_data_ditolak.php File Empty!");
        include 'view/daftarklaim/detail_rekap_data_ditolak.php';
        break;
    case '148'            :
        if (!file_exists ("view/daftarklaim/detail_form_klaim_rawat_jalan.php"))
            die ("daftarklaim/detail_form_klaim_rawat_jalan.php File Empty!");
        include 'view/daftarklaim/detail_form_klaim_rawat_jalan.php';
        break;
    case '149'            :
        if (!file_exists ("view/daftarklaim/detail_form_klaim_rawat_inap.php"))
            die ("daftarklaim/detail_form_klaim_rawat_inap.php File Empty!");
        include 'view/daftarklaim/detail_form_klaim_rawat_inap.php';
        break;
    case '150'            :
        if (!file_exists ("view/daftarklaim/detail_form_rekapitulasi_klaim.php"))
            die ("daftarklaim/detail_form_rekapitulasi_klaim.php File Empty!");
        include 'view/daftarklaim/detail_form_rekapitulasi_klaim.php';
        break;
    case '151'            :
        if (!file_exists ("view/daftarklaim/detail_verifikasi_rawat_jalan.php"))
            die ("daftarklaim/detail_verifikasi_rawat_jalan.php File Empty!");
        include 'view/daftarklaim/detail_verifikasi_rawat_jalan.php';
        break;
    case '152'            :
        if (!file_exists ("view/daftarklaim/detail_verifikasi_rawat_inap.php"))
            die ("daftarklaim/detail_verifikasi_rawat_inap.php File Empty!");
        include 'view/daftarklaim/detail_verifikasi_rawat_inap.php';
        break;
    case '153'            :
        if (!file_exists ("view/daftarklaim/form_pencarian.php"))
            die ("daftarklaim/form_pencarian.php File Empty!");
        include 'view/daftarklaim/form_pencarian.php';
        break;
    case '154'            :
        if (!file_exists ("view/daftarklaim/list_pasien_bersarat.php"))
            die ("daftarklaim/list_pasien_bersarat.php File Empty!");
        include 'view/daftarklaim/list_pasien_bersarat.php';
        break;
    case '156'            :
        if (!file_exists ("view/daftarklaim/hasil_verifikasi_sarat.php"))
            die ("daftarklaim/hasil_verifikasi_sarat.php File Empty!");
        include 'view/daftarklaim/hasil_verifikasi_sarat.php';
        break;
    case '157'            :
        if (!file_exists ("view/daftarklaim/hasil_akhir_verifikasi.php"))
            die ("daftarklaim/hasil_akhir_verifikasi.php File Empty!");
        include 'view/daftarklaim/hasil_akhir_verifikasi.php';
        break;


    //---------------------------------------jamkesmas


    case '16'            :
        if (!file_exists ("view/gizi/dpmp.php"))
            die ("gizi/dpmp.php File Empty!");
        include 'view/gizi/dpmp.php';
        break;
    case '161'            :
        if (!file_exists ("view/gizi/rekap_dpmp.php"))
            die ("gizi/rekap_dpmp.php File Empty!");
        include 'view/gizi/rekap_dpmp.php';
        break;
    case '17a'            :
        if (!file_exists ("view/admission/form_daftarrawatinap.php"))
            die ("admission/form_daftarrawatinap.php File Empty!");
        include 'view/admission/form_daftarrawatinap.php';
        break;
    case '17b'            :
        if (!file_exists ("view/admission/batal_daftar.php"))
            die ("admission/batal_daftar.php File Empty!");
        include 'view/admission/batal_daftar.php';
        break;
    case '17'            :
        if (!file_exists ("view/admission/form_daftar.php"))
            die ("admission/form_daftar.php File Empty!");
        include 'view/admission/form_daftar.php';
        break;
    case '171'            :
        if (!file_exists ("view/admission/list_rawat_inap.php"))
            die ("admission/list_rawat_inap.php File Empty!");
        include 'view/admission/list_rawat_inap.php';
        break;
    case '172'            :
        if (!file_exists ("view/admission/response.php"))
            die ("admission/response.php File Empty!");
        include 'view/admission/response.php';
        break;
    case '173'            :
        if (!file_exists ("view/admission/list_room.php"))
            die ("admission/list_room.php File Empty!");
        include 'view/admission/list_room.php';
        break;
    case '173x'            :
        if (!file_exists ("view/admission/list_room_rujuk.php"))
            die ("admission/list_room_rujuk.php File Empty!");
        include 'view/admission/list_room_rujuk.php';
        break;
    case '174'            :
        if (!file_exists ("view/admission/simpandaftar.php"))
            die ("admission/simpandaftar.php File Empty!");
        include 'view/admission/simpandaftar.php';
        break;
    case '174x'            :
        if (!file_exists ("view/admission/simpandaftar_rujuk.php"))
            die ("admission/simpandaftar_rujuk.php File Empty!");
        include 'view/admission/simpandaftar_rujuk.php';
        break;
    case '175'            :
        if (!file_exists ("view/admission/editadmission.php"))
            die ("admission/editadmission.php File Empty!");
        include 'view/admission/editadmission.php';
        break;
    case '176'            :
        if (!file_exists ("view/admission/list_room_pengganti.php"))
            die ("admission/list_room_pengganti.php File Empty!");
        include 'view/admission/list_room_pengganti.php';
        break;
    case '177'            :
        if (!file_exists ("view/admission/form_pencarian.php"))
            die ("admission/form_pencarian.php File Empty!");
        include 'view/admission/form_pencarian.php';
        break;
    case '178'            :
        if (!file_exists ("view/admission/hasilcariadmission.php"))
            die ("admission/hasilcariadmission.php File Empty!");
        include 'view/admission/hasilcariadmission.php';
        break;
    case '179'            :
        if (!file_exists ("view/admission/sensus_pendaftaran_rawat_inap.php"))
            die ("admission/sensus_pendaftaran_rawat_inap.php File Empty!");
        include 'view/admission/sensus_pendaftaran_rawat_inap.php';
        break;
    case '18'            :
        if (!file_exists ("view/admission/index.php"))
            die ("admission/index.php File Empty!");
        include 'view/admission/index.php';
        break;
    case '19'            :
        if (!file_exists ("view/icd/index.php"))
            die ("icd/index.php File Empty!");
        include 'view/icd/index.php';
        break;
    case '191'            :
        if (!file_exists ("view/icd/edit_icd.php"))
            die ("icd/edit_icd.php File Empty!");
        include 'view/icd/edit_icd.php';
        break;
    case '17f'            :
        if (!file_exists ("view/admission/listpasien_rajal.php"))
            die ("admission/listpasien_rajal.php File Empty!");
        include 'view/admission/listpasien_rajal.php';
        break;
    case '17g'            :
        if (!file_exists ("view/admission/form_daftar_rujuk.php"))
            die ("admission/form_daftar_rujuk.php File Empty!");
        include 'view/admission/form_daftar_rujuk.php';
        break;

    //Kamar Operasi
	
	case 'lapok'            :
        if (!file_exists ("view/operasi/laporan_ok.php"))
            die ("operasi/laporan_ok.php File Empty!");
        include 'view/operasi/laporan_ok.php';
        break;
    case '20'            :
        if (!file_exists ("view/operasi/list_pasien_operasi.php"))
            die ("operasi/list_pasien_operasi.php File Empty!");
        include 'view/operasi/list_pasien_operasi.php';
        break;
    case '201'            :
        if (!file_exists ("view/operasi/form_daftar_operasi.php"))
            die ("operasi/form_daftar_operasi.php File Empty!");
        include 'view/operasi/form_daftar_operasi.php';
        break;
    case '202'            :
        if (!file_exists ("view/operasi/selesai_operasi.php"))
            die ("operasi/selesai_operasi.php File Empty!");
        include 'view/operasi/selesai_operasi.php';
        break;
    case '203'            :
        if (!file_exists ("view/operasi/laporan_operasi.php"))
            die ("operasi/laporan_operasi.php File Empty!");
        include 'view/operasi/laporan_operasi.php';
        break;
    case '204'            :
        if (!file_exists ("view/operasi/tambah_laporan_operasi.php"))
            die ("operasi/tambah_laporan_operasi.php File Empty!");
        include 'view/operasi/tambah_laporan_operasi.php';
        break;
    case '205'            :
        if (!file_exists ("view/operasi/list_rencana_operasi_elektif.php"))
            die ("operasi/list_rencana_operasi_elektif.php File Empty!");
        include 'view/operasi/list_rencana_operasi_elektif.php';
        break;
    case '206'            :
        if (!file_exists ("view/operasi/pemakaian_obat.php"))
            die ("operasi/pemakaian_obat.php File Empty!");
        include 'view/operasi/pemakaian_obat.php';
        break;
    case '207'            :
        if (!file_exists ("view/operasi/tindakan_medis.php"))
            die ("operasi/tindakan_medis.php File Empty!");
        include 'view/operasi/tindakan_medis.php';
        break;
    case '208'            :
        if (!file_exists ("view/operasi/pilih_pemakaian_obat.php"))
            die ("operasi/pilih_pemakaian_obat.php File Empty!");
        include 'view/operasi/pilih_pemakaian_obat.php';
        break;
    case '209'            :
        if (!file_exists ("view/operasi/pilih_tindakan_medis.php"))
            die ("operasi/pilih_tindakan_medis.php File Empty!");
        include 'view/operasi/pilih_tindakan_medis.php';
        break;
	case 'tindakan_operasi'            :
        if (!file_exists ("view/operasi/form_tindakan_operasi.php"))
            die ("operasi/form_tindakan_operasi.php File Empty!");
        include 'view/operasi/form_tindakan_operasi.php';
        break;
	case 'tindakan_operasilain'            :
        if (!file_exists ("view/operasi/form_tindakan_operasilain.php"))
            die ("operasi/form_tindakan_operasilain.php File Empty!");
        include 'view/operasi/form_tindakan_operasilain.php';
        break;
	case 'setting_dokter_operasi'            :
        if (!file_exists ("view/operasi/setting_dokter_operasi.php"))
            die ("operasi/setting_dokter_operasi.php File Empty!");
        include 'view/operasi/setting_dokter_operasi.php';
        break;
    case 'op1'            :
        if (!file_exists ("view/operasi/orderbarang/pengeluaranbarang.php"))
            die ("operasi/orderbarang/pengeluaranbarang.php File Empty!");
        include 'view/operasi/orderbarang/pengeluaranbarang.php';
        break;
    case 'x206'            :
        if (!file_exists ("view/operasi/orderbarang/batalpengeluaranbarang.php"))
            die ("operasi/orderbarang/batalpengeluaranbarang.php File Empty!");
        include 'view/operasi/orderbarang/batalpengeluaranbarang.php';
        break;
    case 'x209'            :
        if (!file_exists ("view/operasi/bataltindakanmedis.php"))
            die ("operasi/bataltindakanmedis.php File Empty!");
        include 'view/operasi/bataltindakanmedis.php';
        break;

//multi user - pendaftaran ke admission		

    case 'ad1'            :
        if (!file_exists ("view/admission/form_daftarrawatinap.php"))
            die ("admission/form_daftarrawatinap.php File Empty!");
        unset($_SESSION['ROLES']);
        $_SESSION['ROLES']=="ad1";
        include 'view/admission/form_daftarrawatinap.php';
        break;
    case 'ad2'            :
        if (!file_exists ("view/Pendaftaran.php"))
            die ("Pendaftaran.php File Empty!");
        unset($_SESSION['ROLES']);
        $_SESSION['ROLES']=="1";
        include 'view/Pendaftaran.php';
        break;

//askes

    case '21'            :
        if (!file_exists ("view/askes/billaskes.php"))
            die ("askes/billaskes.php File Empty!");
        include 'view/askes/billaskes.php';
        break;
    case 'As01'            :
        if (!file_exists ("view/askes/billaskes.php"))
            die ("askes/billaskes.php File Empty!");
        include 'view/askes/billaskes.php';
        break;
    case 'As03'            :
        if (!file_exists ("view/askes/cartbill.php"))
            die ("askes/cartbill.php File Empty!");
        include 'view/askes/cartbill.php';
        break;

    //kamar bersalin
    case 'v01'            :
        if (!file_exists ("view/vk/lap_reg_partus.php"))
            die ("vk/lap_reg_partus.php File Empty!");
        include 'view/vk/lap_reg_partus.php';
        break;
    case 'v02'            :
        if (!file_exists ("view/vk/lap_harian_vk.php"))
            die ("vk/lap_harian_vk.php File Empty!");
        include 'view/vk/lap_harian_vk.php';
        break;
    case 'v03'            :
        if (!file_exists ("view/vk/list_rencana_operasi_elektif.php"))
            die ("vk/list_rencana_operasi_elektif.php File Empty!");
        include 'view/vk/list_rencana_operasi_elektif.php';
        break;
    case 'v04'            :
        if (!file_exists ("view/vk/list_rencana_operasi_cito.php"))
            die ("vk/list_rencana_operasi_cito.php File Empty!");
        include 'view/vk/list_rencana_operasi_cito.php';
        break;
    case 'v05'            :
        if (!file_exists ("view/vk/operasi/del_kuret.php"))
            die ("vk/operasi/del_kuret.php File Empty!");
        include 'view/vk/operasi/del_kuret.php';
        break;

    //keuangan
     //keuangan
    case '24k'            :
        if (!file_exists ("view/keuangan/index.php"))
            die ("keuangan/index.php File Empty!");
        include 'view/keuangan/index.php';
        break;
    case '24k1'            :
        if (!file_exists ("view/keuangan/setup/perkiraan.php"))
            die ("keuangan/setup/perkiraan.php File Empty!");
        include 'view/keuangan/setup/perkiraan.php';
        break;
    case '24k2'            :
        if (!file_exists ("view/keuangan/setup/realisasi_anggaran.php"))
            die ("keuangan/setup/realisasi_anggaran.php File Empty!");
        include 'view/keuangan/setup/realisasi_anggaran.php';
        break;		
    case '24k3'            :
        if (!file_exists ("view/keuangan/setup/arus_kas.php"))
            die ("keuangan/setup/arus_kas.php File Empty!");
        include 'view/keuangan/setup/arus_kas.php';
        break;			
    case '24k4'            :
        if (!file_exists ("view/keuangan/setup/tarif.php"))
            die ("keuangan/setup/tarif.php File Empty!");
        include 'view/keuangan/setup/tarif.php';
        break;			
    case '31k1'            :
        if (!file_exists ("view/keuangan/entri/perkiraan.php"))
            die ("keuangan/entri/perkiraan.php File Empty!");
        include 'view/keuangan/entri/perkiraan.php';
        break;
    case '31k2'            :
        if (!file_exists ("view/keuangan/entri/realisasi_anggaran.php"))
            die ("keuangan/entri/realisasi_anggaran.php File Empty!");
        include 'view/keuangan/entri/realisasi_anggaran.php';
        break;		
    case '31k3'            :
        if (!file_exists ("view/keuangan/entri/arus_kas.php"))
            die ("keuangan/entri/arus_kas.php File Empty!");
        include 'view/keuangan/entri/arus_kas.php';
        break;			
    case '35k1'            :
        if (!file_exists ("view/keuangan/laporan/perkiraan.php"))
            die ("keuangan/laporan/perkiraan.php File Empty!");
        include 'view/keuangan/laporan/perkiraan.php';
        break;	
    case '35k2'            :
        if (!file_exists ("view/keuangan/laporan/realisasi_anggaran.php"))
            die ("keuangan/laporan/realisasi_anggaran.php File Empty!");
        include 'view/keuangan/laporan/realisasi_anggaran.php';
        break;			
    case '35k3'            :
        if (!file_exists ("view/keuangan/laporan/arus_kas.php"))
            die ("keuangan/laporan/arus_kas.php File Empty!");
        include 'view/keuangan/laporan/arus_kas.php';
        break;			
    case '36k1'            :
        if (!file_exists ("view/keuangan/pendapatan_sirs/pendapatan_unit.php"))
            die ("keuangan/pendapatan_sirs/pendapatan_unit.php File Empty!");
        include 'view/keuangan/pendapatan_sirs/pendapatan_unit.php';
        break;
    case '36k2'            :
        if (!file_exists ("view/keuangan/pendapatan_sirs/pendapatan_percarabayar.php"))
            die ("keuangan/pendapatan_sirs/pendapatan_percarabayar.php File Empty!");
        include 'view/keuangan/pendapatan_sirs/pendapatan_percarabayar.php';
        break;	

//Jaspel
	case 'jas0'            :
        if (!file_exists ("view/jaspel/jaspel_setting.php"))
            die ("jaspel/jaspel_setting.php File Empty!");
        include 'view/jaspel/jaspel_setting.php';
        break;
	case 'jas10'            :
        if (!file_exists ("view/jaspel/jaspel_rekap_all.php"))
            die ("jaspel/jaspel_rekap_all.php File Empty!");
        include 'view/jaspel/jaspel_rekap_all.php';
        break;
    case 'jas1'            :
        if (!file_exists ("view/jaspel/jaspel_rj.php"))
            die ("jaspel/jaspel_rj.php File Empty!");
        include 'view/jaspel/jaspel_rj.php';
        break;
    case 'jas2'            :
        if (!file_exists ("view/jaspel/jaspel_ok.php"))
            die ("jaspel/jaspel_ok.php File Empty!");
        include 'view/jaspel/jaspel_ok.php';
        break;
    case 'jas3'            :
        if (!file_exists ("view/jaspel/jaspel_ugd.php"))
            die ("jaspel/jaspel_ugd.php File Empty!");
        include 'view/jaspel/jaspel_ugd.php';
        break;
    case 'jas4'            :
        if (!file_exists ("view/jaspel/jaspel_ranap.php"))
            die ("jaspel/jaspel_ranap.php File Empty!");
        include 'view/jaspel/jaspel_ranap.php';
        break;
    case 'jas5'            :
        if (!file_exists ("view/jaspel/jaspel_lab.php"))
            die ("jaspel/jaspel_lab.php File Empty!");
        include 'view/jaspel/jaspel_lab.php';
        break;
    case 'jas6'            :
        if (!file_exists ("view/jaspel/jaspel_rad.php"))
            die ("jaspel/jaspel_rad.php File Empty!");
        include 'view/jaspel/jaspel_rad.php';
        break;
    case 'jas7'            :
        if (!file_exists ("view/jaspel/jaspel_manajemen.php"))
            die ("jaspel/jaspel_manajemen.php File Empty!");
        include 'view/jaspel/jaspel_manajemen.php';
        break;
    case 'jas8'            :
        if (!file_exists ("view/jaspel/jaspel_pendukung.php"))
            die ("jaspel/jaspel_pendukung.php File Empty!");
        include 'view/jaspel/jaspel_pendukung.php';
        break;
    case 'jas9'            :
        if (!file_exists ("view/jaspel/jaspel_vk.php"))
            die ("jaspel/jaspel_vk.php File Empty!");
        include 'view/jaspel/jaspel_vk.php';
        break;
    //jadwal dokter
    case 'jdoc'            :
        if (!file_exists ("view/jadwaldokter/lihatjadwal.php"))
            die ("jadwaldokter/lihatjadwal.php File Empty!");
        include 'view/jadwaldokter/lihatjadwal.php';
        break;
    case 'jdoc2'            :
        if (!file_exists ("view/jadwaldokter/form_addjadwal.php"))
            die ("jadwaldokter/form_addjadwal.php File Empty!");
        include 'view/jadwaldokter/form_addjadwal.php';
        break;
    case 'jdoc3'            :
        if (!file_exists ("view/jadwaldokter/hasillihatjadwaldokter.php"))
            die ("jadwaldokter/hasillihatjadwaldokter.php File Empty!");
        include 'view/jadwaldokter/hasillihatjadwaldokter.php';
        break;
    case 'jdoc4'            :
        if (!file_exists ("view/jadwaldokter/addjadwal.php"))
            die ("jadwaldokter/addjadwal.php File Empty!");
        include 'view/jadwaldokter/addjadwal.php';
        break;

    case 'jdoc4'            :
        if (!file_exists ("view/batal_poly.php"))
            die ("batal_poly.php File Empty!");
        include 'view/batal_poly.php';
        break;

    case 'batal_r'            :
        if (!file_exists ("view/rajal/batal_pasien.php"))
            die ("rajal/batal_pasien.php File Empty!");
        include 'view/rajal/batal_pasien.php';
        break;

    case 'batal_v'            :
        if (!file_exists ("view/vk/batal_pasien.php"))
            die ("vk/batal_pasien.php File Empty!");
        include 'view/vk/batal_pasien.php';
        break;

    case 'batal_u'            :
        if (!file_exists ("view/ugd/batal_pasien.php"))
            die ("ugd/batal_pasien.php File Empty!");
        include 'view/ugd/batal_pasien.php';
        break;
		
	case 'setting_dokter':
		if (!file_exists ("setting_dokter.php"))
            die ("setting_dokter.php File Empty!");
        include 'view/setting_dokter.php';
        break;
	case 'adminrajal':
		if (!file_exists ("adminrajal/adminrajal.php"))
            die ("adminrajal/adminrajal.php File Empty!");
        include 'view/adminrajal/adminrajal.php';
        break;
	case 'adminrajal_aps':
		if (!file_exists ("adminrajal/adminrajal_aps.php"))
            die ("adminrajal/adminrajal_aps.php File Empty!");
        include 'view/adminrajal/adminrajal_aps.php';
        break;
	case 'adminrajal_daftar_aps':
		if (!file_exists ("adminrajal/daftar_aps.php"))
            die ("adminrajal/daftar_aps.php File Empty!");
        include 'view/adminrajal/daftar_aps.php';
        break;
	case 'adminrajal_diagnosis':
		if (!file_exists ("adminrajal/adminrajal_diagnosis.php"))
            die ("adminrajal/adminrajal_diagnosis.php File Empty!");
        include 'view/adminrajal/adminrajal_diagnosis.php';
        break;
	case 'detail_billing':
		if (!file_exists ("detail_billing.php"))
            die ("detail_billing.php File Empty!");
        include 'view/detail_billing.php';
        break;
	case 'list_pasien_ranap_lab':
		if (!file_exists ("lab/list_pasien_ranap_lab.php"))
            die ("lab/list_pasien_ranap_lab.php File Empty!");
        include 'view/lab/list_pasien_ranap_lab.php';
        break;
	case 'list_pasien_rajal_lab':
		if (!file_exists ("lab/list_pasien_rajal_lab.php"))
            die ("lab/list_pasien_rajal_lab.php File Empty!");
        include 'view/lab/list_pasien_rajal_lab.php';
        break;
	case '62formorderlab_ranap':
		if (!file_exists ("lab/formorderlab_ranap.php"))
            die ("lab/formorderlab_ranap.php File Empty!");
        include 'view/lab/formorderlab_ranap.php';
        break;
	case '62formorderlab_rajal':
		if (!file_exists ("lab/formorderlab_rajal.php"))
            die ("lab/formorderlab_rajal.php File Empty!");
        include 'view/lab/formorderlab_rajal.php';
        break;
	case 'list_billing_ranap':
		if (!file_exists ("admission/list_billing_ranap.php"))
            die ("admission/list_billing_ranap.php File Empty!");
        include 'view/admission/list_billing_ranap.php';
        break;
	case 'detail_billing_ranap':
		if (!file_exists ("admission/detail_billing_ranap.php"))
            die ("admission/detail_billing_ranap.php File Empty!");
        include 'view/admission/detail_billing_ranap.php';
        break;

//keperawatan
	case 'list_kep':
		if (!file_exists ("kep/list_data_perawat.php"))
            die ("list_data_perawat.php File Empty!");
        include 'view/kep/list_data_perawat.php';
        break;
	case 'kep2'            :
        if (!file_exists ("view/kep/add_edit_m_perawat.php"))
            die ("add_edit_m_perawat.php File Empty!");
        include 'view/kep/add_edit_m_perawat.php';
        break;
	case 'kep3':
        if (!file_exists ("view/kep/mutasi_perawat.php"))
            die ("mutasi_perawat.php File Empty!");
        include 'view/kep/mutasi_perawat.php';
        break;
	case 'kep4':
        if (!file_exists ("view/kep/keluar_perawat.php"))
            die ("keluar_perawat.php File Empty!");
        include 'view/kep/keluar_perawat.php';
        break;
	case 'kep5':
        if (!file_exists ("view/kep/program_pengembangan.php"))
            die ("program_pengembangan.php File Empty!");
        include 'view/kep/program_pengembangan.php';
        break;
	case 'sdm_kep':
        if (!file_exists ("view/kep/sdm_keperawatan.php"))
            die ("sdm_keperawatan.php File Empty!");
        include 'view/kep/sdm_keperawatan.php';
        break;
	case 'askep__'            :
        if (!file_exists ("view/kep/list_askep.php"))
            die ("kep/list_askep.php File Empty!");
        include 'view/kep/list_askep.php';
        break;
	case 'met_gas':
        if (!file_exists ("view/kep/metode_penugasan.php"))
            die ("metode_penugasan.php File Empty!");
        include 'view/kep/metode_penugasan.php';
        break;
	case 'supvis':
        if (!file_exists ("view/kep/supervisi.php"))
            die ("supervisi.php File Empty!");
        include 'view/kep/supervisi.php';
        break;
	case 'lap_ranap_kep':
        if (!file_exists ("view/kep/lap_rawat_inap.php"))
            die ("lap_rawat_inap.php File Empty!");
        include 'view/kep/lap_rawat_inap.php';
        break;
	case 'pengkajian_kep'            :
        if (!file_exists ("view/kep/kajian_kep.php"))
            die ("kajian_keperawatan.php File Empty!");
        include 'view/kep/kajian_kep.php';
        break;
	case 'diagnosa_kep'            :
        if (!file_exists ("view/kep/diagnosa_kep.php"))
            die ("diagnosa_keperawatan.php File Empty!");
        include 'view/kep/diagnosa_kep.php';
        break;
	
	#### NEW ###
	case 'createCaseMix':
		if (!file_exists ("adm/eksekutif/slide/casemix.php"))
            die ("adm/eksekutif/slide/casemix.php File Empty!");
        include 'view/adm/eksekutif/slide/casemix.php';
        break;
	
	case 'pendapatan_cash'            :
        if (!file_exists ("view/keuangan/setup/pend_cash.php"))
            die ("keuangan/setup/pend_cash.php File Empty!");
        include 'view/keuangan/setup/pend_cash.php';
        break;
	case 'pendapatan_piutang'            :
        if (!file_exists ("view/keuangan/setup/pend_piutang.php"))
            die ("keuangan/setup/pend_piutang.php File Empty!");
        include 'view/keuangan/setup/pend_piutang.php';
        break;
	case 'general_ledger'            :
        if (!file_exists ("view/keuangan/entri/general_ledger.php"))
            die ("keuangan/entri/general_ledger.php File Empty!");
        include 'view/keuangan/entri/general_ledger.php';
        break;
	case 'laporan_hutang'            :
        if (!file_exists ("view/keuangan/entri/laporan_hutang.php"))
            die ("keuangan/entri/laporan_hutang.php File Empty!");
        include 'view/keuangan/entri/laporan_hutang.php';
        break;
	case 'telepon'            :
        if (!file_exists ("view/daftar_.php"))
            die ("daftar.php File Empty!");
        include 'view/daftar_.php';
        break;
	case 'praktek_dokter'            :
        if (!file_exists ("view/praktek_dokter.php"))
            die ("praktek_dokter.php File Empty!");
        include 'view/praktek_dokter.php';
        break;
		
	case 'rl11'            :
        if (!file_exists ("view/rm/rl11.php"))
            die ("rm/rl11.php File Empty!");
        include 'view/rm/rl11.php';
        break;
	case 'rl12'            :
        if (!file_exists ("view/rm/rl12.php"))
            die ("rm/rl12.php File Empty!");
        include 'view/rm/rl12.php';
        break;
	case 'rl13'            :
        if (!file_exists ("view/rm/rl13.php"))
            die ("rm/rl13.php File Empty!");
        include 'view/rm/rl13.php';
        break;	
	case 'rl2'            :
        if (!file_exists ("view/rm/rl2.php"))
            die ("rm/rl2.php File Empty!");
        include 'view/rm/rl2.php';
        break;
	case 'rl31'            :
        if (!file_exists ("view/rm/rl31.php"))
            die ("rm/rl31.php File Empty!");
        include 'view/rm/rl31.php';
        break;
	case 'rl32'            :
        if (!file_exists ("view/rm/rl32.php"))
            die ("rm/rl32.php File Empty!");
        include 'view/rm/rl32.php';
        break;
	case 'rl33'            :
        if (!file_exists ("view/rm/rl33.php"))
            die ("rm/rl33.php File Empty!");
        include 'view/rm/rl33.php';
        break;
	case 'rl34'            :
        if (!file_exists ("view/rm/rl34.php"))
            die ("rm/rl34.php File Empty!");
        include 'view/rm/rl34.php';
        break;
	case 'rl35'            :
        if (!file_exists ("view/rm/rl35.php"))
            die ("rm/rl35.php File Empty!");
        include 'view/rm/rl35.php';
        break;
	case 'rl36'            :
        if (!file_exists ("view/rm/rl36.php"))
            die ("rm/rl36.php File Empty!");
        include 'view/rm/rl36.php';
        break;
	case 'rl37'            :
        if (!file_exists ("view/rm/rl37.php"))
            die ("rm/rl37.php File Empty!");
        include 'view/rm/rl37.php';
        break;
	case 'rl38'            :
        if (!file_exists ("view/rm/rl38.php"))
            die ("rm/rl38.php File Empty!");
        include 'view/rm/rl38.php';
        break;
	case 'rl39'            :
        if (!file_exists ("view/rm/rl39.php"))
            die ("rm/rl39.php File Empty!");
        include 'view/rm/rl39.php';
        break;
	case 'rl310'            :
        if (!file_exists ("view/rm/rl310.php"))
            die ("rm/rl310.php File Empty!");
        include 'view/rm/rl310.php';
        break;
	case 'rl311'            :
        if (!file_exists ("view/rm/rl311.php"))
            die ("rm/rl311.php File Empty!");
        include 'view/rm/rl311.php';
        break;
	case 'rl312'            :
        if (!file_exists ("view/rm/rl312.php"))
            die ("rm/rl312.php File Empty!");
        include 'view/rm/rl312.php';
        break;
	case 'rl313'            :
        if (!file_exists ("view/rm/rl313.php"))
            die ("rm/rl313.php File Empty!");
        include 'view/rm/rl313.php';
        break;
	case 'rl314'            :
        if (!file_exists ("view/rm/rl314.php"))
            die ("rm/rl314.php File Empty!");
        include 'view/rm/rl314.php';
        break;
	case 'rl315'            :
        if (!file_exists ("view/rm/rl315.php"))
            die ("rm/rl315.php File Empty!");
        include 'view/rm/rl315.php';
        break;
	case 'rl51'            :
        if (!file_exists ("view/rm/rl51.php"))
            die ("rm/rl51.php File Empty!");
        include 'view/rm/rl51.php';
        break;
	case 'rl52'            :
        if (!file_exists ("view/rm/rl52.php"))
            die ("rm/rl52.php File Empty!");
        include 'view/rm/rl52.php';
        break;
	case 'rl5310'            :
        if (!file_exists ("view/rm/rl5310.php"))
            die ("rm/rl5310.php File Empty!");
        include 'view/rm/rl5310.php';
        break;
	case 'rl5410'            :
        if (!file_exists ("view/rm/rl5410.php"))
            die ("rm/rl5410.php File Empty!");
        include 'view/rm/rl5410.php';
        break;
}


?>
